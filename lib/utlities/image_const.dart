const String _svgPath = 'assets/svg/';
const String _iconPath = 'assets/icons/';
const String _imagePath = 'assets/images/';

/// png asset
const String appLogo = _imagePath + 'appLogo.png';
const String egyptIcon = _imagePath + 'egyptIcon.png';

/// svg asset
// const String appLogo = _svgPath + 'logo.svg';
const String arrowDownIcon = _svgPath + 'arrowDown.svg';
const String coPolitanBlockLogo = _svgPath + 'coPolitanBlockLogo.svg';
const String automatedCallIcon = _svgPath + 'automatedCallIcon.svg';
const String smsIcon = _svgPath + 'smsIcon.svg';
