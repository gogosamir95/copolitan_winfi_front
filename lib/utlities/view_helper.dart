import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/widgets/custom_progress.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_custom_tabs/flutter_custom_tabs.dart';
import 'package:progress_dialog_null_safe/progress_dialog_null_safe.dart';

import 'app_color.dart';
import 'custom_text_style.dart';
import 'logger.dart';

class ViewHelper {
  final BuildContext context;
  final bool fullScreen;
  ProgressDialog? _progressDialog;

  ViewHelper(this.context, {this.fullScreen = false});

  ViewHelper initProgressDialog() {
    _progressDialog = ProgressDialog(context,
        showLogs: true, isDismissible: true, type: ProgressDialogType.normal);
    _initProgressDialogStyle();
    return this;
  }

  void _initProgressDialogStyle() {
    _progressDialog!.style(
        progressWidget: CustomProgress(
          size: 50,
        ),
        elevation: 3,
        borderRadius: 5,
        insetAnimCurve: Curves.fastLinearToSlowEaseIn,
        backgroundColor: whiteColor,
        message: S.of(context).pleaseWait,
        padding: EdgeInsets.symmetric(
            horizontal: MediaQuery.of(context).size.width * 0.3),
        messageTextStyle:
            SemiBoldStyle(color: blackColor70Percent, fontSize: 18).getStyle(),
        progressTextStyle:
            SemiBoldStyle(color: blackColor70Percent, fontSize: 18).getStyle(),
        progressWidgetAlignment: Alignment.center,
        widgetAboveTheDialog: CustomText(
          text: S.of(context).loading,
          customTextStyle: SemiBoldStyle(color: accentColor, fontSize: 18),
        ),
        textAlign: TextAlign.center);
  }

  void showProgressDialog() {
    if (!_progressDialog!.isShowing()) _progressDialog!.show();
  }

  void hideProgress() {
    if (_progressDialog!.isShowing()) _progressDialog!.hide();
  }

  void showDialogWithAnimation(Widget widget) {
    showGeneralDialog(
        context: context,
        transitionDuration: Duration(milliseconds: 50),
        barrierDismissible: false,
        barrierLabel: '',
        barrierColor: Colors.black.withOpacity(0.5),
        transitionBuilder: (context, a1, a2, widget) {
          return Transform.scale(
            scale: a1.value,
            child: Opacity(opacity: a1.value, child: widget),
          );
        },
        pageBuilder: (context, animation1, animation2) => widget);
  }

  void showCustomBottomSheetFullScreen(Widget child,
      {Function(dynamic result)? onUpdate}) {
    // Navigator.of(context)
    //     .push(PageRouteBuilder(
    //         reverseTransitionDuration: Duration(milliseconds: 200),
    //         transitionDuration: Duration(milliseconds: 200),
    //         opaque: false,
    //         transitionsBuilder:
    //             (context, animation, secondaryAnimation, child) {
    //           var begin = Offset(0.0, 1.0);
    //           var end = Offset.zero;
    //           var tween = Tween(begin: begin, end: end);
    //           var offsetAnimation = animation.drive(tween);
    //           return SlideTransition(
    //             position: offsetAnimation,
    //             child: child,
    //           );
    //         },
    //         pageBuilder: (context, animation, secondaryAnimation) =>
    //             CustomBottomSheetDialog(
    //               child: child,
    //               fullScreen: fullScreen,
    //             )))
    //     .then((value) {
    //   if (onUpdate != null) onUpdate(value);
    // });
    // showAnimatedDialog(
    //         context: context,
    //         builder: (context) => CustomBottomSheetDialog(
    //               child: child,
    //               fullScreen: fullScreen,
    //             ),
    //         curve: Curves.fastOutSlowIn,
    //         alignment: Alignment.bottomCenter,
    //         axis: Axis.vertical,
    //         animationType: DialogTransitionType.slideFromBottomFade,
    //         duration: Duration(milliseconds: 500),
    //         barrierDismissible: false,
    //         barrierColor: Colors.transparent)
    //     .then((value) {
    //   if (onUpdate != null) onUpdate(value);
    // });
  }

  showSnakeBar(String text,
      {SnackBarAction? action,
      Color snakeColorBackGround = accentColor,
      Color textColor = Colors.white,
      double elevation = 0.0,
      int durationInMilliSeconds = 2000}) async {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: CustomText(
        text: text,
        customTextStyle: SemiBoldStyle(fontSize: 14, color: whiteColor),
      ),
      backgroundColor: snakeColorBackGround,
      elevation: elevation,
      behavior: SnackBarBehavior.floating,
      action: action,
      duration: Duration(milliseconds: durationInMilliSeconds),
      padding: EdgeInsets.only(left: 16.0, right: 16.0, top: 12, bottom: 12),
      margin: EdgeInsets.zero,
    ));
  }

  Future<dynamic> pushScreen(dynamic widget, {bool hasBackStack = false}) {
    // TransitionsPage(
    //     child: widget,
    //     context: context,
    //     animation: AnimationType.fadeIn,
    //     duration: Duration(milliseconds: 100),
    //     replacement: hasBackStack);
    if (!hasBackStack)
      return Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => widget,
          ));
    else
      return Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => widget,
          ));
  }

  void launchURL(String url) async {
    try {
      await launch(
        url,
        customTabsOption: CustomTabsOption(
          toolbarColor: primaryColor,
          enableDefaultShare: true,
          enableUrlBarHiding: true,
          showPageTitle: true,
          extraCustomTabs: const <String>[
            // ref. https://play.google.com/store/apps/details?id=org.mozilla.firefox
            'org.mozilla.firefox',
            // ref. https://play.google.com/store/apps/details?id=com.microsoft.emmx
            'com.microsoft.emmx',
          ],
        ),
        safariVCOption: SafariViewControllerOption(
          preferredBarTintColor: primaryColor,
          preferredControlTintColor: accentColor,
          barCollapsingEnabled: true,
          entersReaderIfAvailable: false,
          dismissButtonStyle: SafariViewControllerDismissButtonStyle.close,
        ),
      );
    } catch (e) {
      Logger.log(message: e.toString(), name: 'open url custom tab error: ');
    }
  }

  void launchUrkInSamePage(String userName, String password) async {}
}
