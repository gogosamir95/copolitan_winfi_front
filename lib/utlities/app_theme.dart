import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'app_color.dart';
import 'custom_text_style.dart';

class AppTheme {
  static final AppTheme _appTheme = AppTheme.internal();

  factory AppTheme() {
    return _appTheme;
  }

  AppTheme.internal();

  ThemeData getTheme(context) => lightTheme;

  ThemeData get lightTheme => ThemeData(
        brightness: Brightness.dark,
        // textTheme: setAppTextTheme(),
        backgroundColor: Colors.white,
        scaffoldBackgroundColor: Colors.white,
        dialogBackgroundColor: Colors.white,
        disabledColor: greyColor,
        hintColor: greyColor70Percent,
        unselectedWidgetColor: greyColor,
        primaryColor: Colors.white,
        primarySwatch: Colors.grey,
        accentColor: primaryColor,
        errorColor: primaryColor,
        buttonColor: primaryColor,
        toggleableActiveColor: primaryColor,
        canvasColor: Colors.transparent,
        buttonTheme: _buttonTheme,
        shadowColor: whiteColor,
        snackBarTheme: _snackBarThemeData,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      );

  SnackBarThemeData get _snackBarThemeData => SnackBarThemeData(
      actionTextColor: whiteColor,
      backgroundColor: blackColor,
      behavior: SnackBarBehavior.floating,
      elevation: 5.0,
      contentTextStyle:
          RegularStyle(fontSize: 15.0, color: primaryColor).getStyle());

//   setAppTextTheme() {
// //     subtitle1 is used for textFormFiled TextStyle
//     return TextTheme(
//       button: _buttonTextStyle,
//       subtitle1: _subTitleTextStyle,
//       headline1: _headLine1TextStyle,
//       headline2: _headLine2TextStyle,
//     );
//   }

  // TextStyle get _subTitleTextStyle => CustomTextStyle(
  //         textType: TextType.light,
  //         color: darkGreyColor,
  //         fontSize: smallFontSize)
  //     .textStyle;
  //
  // TextStyle get _buttonTextStyle => CustomTextStyle(
  //         textType: TextType.medium,
  //         color: Colors.white,
  //         fontSize: mediumFontSize)
  //     .textStyle;
  //
  // TextStyle get _headLine1TextStyle => CustomTextStyle(
  //         textType: TextType.regular,
  //         color: colorAccent,
  //         fontSize: mediumFontSize)
  //     .textStyle;
  //
  // TextStyle get _headLine2TextStyle => CustomTextStyle(
  //         textType: TextType.regular,
  //         color: colorAccent,
  //         fontSize: mediumFontSize)
  //     .textStyle;

  SystemUiOverlayStyle getSystemUiOverLayStyle(
      {Color color = primaryColor, Brightness brightness = Brightness.dark}) {
    return SystemUiOverlayStyle(
        statusBarColor: color,
        systemNavigationBarColor: color,
        systemNavigationBarIconBrightness: brightness,
        statusBarIconBrightness: brightness,
        statusBarBrightness: brightness,
        systemNavigationBarDividerColor: color);
  }

  ButtonThemeData get _buttonTheme => ButtonThemeData(
        buttonColor: primaryColor,
        shape: new RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(6.0)),
        highlightColor: primaryColor,
        disabledColor: primaryColor,
      );
}
