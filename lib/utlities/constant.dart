class Constant {
  static String appLanguage = 'en';
  static const String baseLiveUrl = 'https://Api.winfico.com:3000/';
  static const String baseDebugUrl = 'https://Api.winfico.com:3000/';
  static const String infoBipBaseUrl = 'https://8wrqr.api.infobip.com/';
  static const bool appLive = false;
  static const int apiVersion = 1;
  static const String mobileRegex = '^(012|015|011|010)([0-9]{8})\$';
  static const String baseUrl = 'https://api.copolitan.com/';
  static const String payTabLiveServerKey = '*server key*';
  static const String payTabDebugServerKey = 'SMJNTB9ZRN-JBNRWRJH6G-9BZDMLZ6ZR';
  static const String payTabLiveClientKey = '*client key*';
  static const String payTabDebugClientKey = 'C9KMKN-MRVG62-QRVR6H-6VRBPT';
  static const String payTapLiveProfileId = '69928';
  static const String payTapDebugProfileId = '69928';
  static const int paidBookingStatus = 1;
  static const int unPaidBookingStatus = 2;
  static const int bookingTypeWorkSpace = 1;
  static const int bookingTypeMeetingRoom = 2;
  static const int bookingTypeBranchInMunch = 3;
  static const int onlinePaymentMethod = 1;
  static const int PayOnSitePaymentMethod = 2;
  static const int hourlyId = 1;
  static const int tailoredId = 2;
  static const String payTabBaseUrl = 'https://secure-egypt.paytabs.com/';
  static const String registerUrl = 'https://www.copolitan.com/#spaces';
  static const int foodicOrderDinInType = 1;
}
