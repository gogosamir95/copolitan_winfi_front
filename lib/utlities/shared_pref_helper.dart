import 'package:copolitan_winfi/enum/shared_pref_enum.dart';
import 'package:simple_shared_pref/simple_shared_pref.dart';

class SharedPRefHelper {
  bool get isLight =>
      SimpleSharedPref().getBool(key: SharedPrefEnum.isLight.toString())!;

  bool get notificationEnable {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.notification.toString()))
      return SimpleSharedPref()
          .getBool(key: SharedPrefEnum.notification.toString())!;
    return false;
  }

  int get userId {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.userId.toString()))
      return SimpleSharedPref().getInt(key: SharedPrefEnum.userId.toString())!;
    return 0;
  }

  bool get firstTimeLogin {
    if (SimpleSharedPref()
        .contain(key: SharedPrefEnum.firstTimeLogin.toString()))
      return SimpleSharedPref()
          .getBool(key: SharedPrefEnum.firstTimeLogin.toString())!;
    return false;
  }

  String get getBearerToken {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.bearerToken.toString()))
      return SimpleSharedPref()
          .getString(key: SharedPrefEnum.bearerToken.toString())!;
    return '';
  }

  String get userName {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.userName.toString()))
      return SimpleSharedPref()
          .getString(key: SharedPrefEnum.userName.toString())!;
    return '';
  }

  String get mobile {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.userMobile.toString()))
      return SimpleSharedPref()
          .getString(key: SharedPrefEnum.userMobile.toString())!;
    return '';
  }

  int get roomId {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.roomId.toString()))
      return SimpleSharedPref().getInt(key: SharedPrefEnum.roomId.toString())!;
    return 0;
  }

  String get email {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.userEmail.toString()))
      return SimpleSharedPref()
          .getString(key: SharedPrefEnum.userEmail.toString())!;
    return '';
  }

  String get userProfileUrl {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.profileUrl.toString()))
      return SimpleSharedPref()
          .getString(key: SharedPrefEnum.profileUrl.toString())!;
    return '';
  }

  bool get userActive {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.active.toString()))
      return SimpleSharedPref().getBool(key: SharedPrefEnum.active.toString())!;
    return false;
  }

  String get customerId {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.customerId.toString()))
      return SimpleSharedPref()
          .getString(key: SharedPrefEnum.customerId.toString())!;
    return '';
  }

  String get branchId {
    if (SimpleSharedPref().contain(key: SharedPrefEnum.BranchId.toString()))
      return SimpleSharedPref()
          .getString(key: SharedPrefEnum.BranchId.toString())!;
    return '';
  }

  void setUserId(int userId) {
    SimpleSharedPref()
        .setInt(value: userId, key: SharedPrefEnum.userId.toString());
  }

  void setNotification(bool value) {
    SimpleSharedPref()
        .setBool(key: SharedPrefEnum.notification.toString(), value: value);
  }

  void setDarkMode(bool value) {
    SimpleSharedPref()
        .setBool(key: SharedPrefEnum.isLight.toString(), value: value);
  }

  void setLanguage(String value) {
    SimpleSharedPref()
        .setString(key: SharedPrefEnum.language.toString(), value: value);
  }

  void setUserName(String value) {
    SimpleSharedPref()
        .setString(key: SharedPrefEnum.userName.toString(), value: value);
  }

  void setEmail(String value) {
    SimpleSharedPref()
        .setString(key: SharedPrefEnum.userEmail.toString(), value: value);
  }

  void setMobile(String value) {
    SimpleSharedPref()
        .setString(key: SharedPrefEnum.userMobile.toString(), value: value);
  }

  void setRoomId(int value) {
    SimpleSharedPref()
        .setInt(value: roomId, key: SharedPrefEnum.roomId.toString());
  }

  void setProfileUrl(String value) {
    SimpleSharedPref()
        .setString(key: SharedPrefEnum.profileUrl.toString(), value: value);
  }

  void setBearerToken(String value) {
    SimpleSharedPref()
        .setString(key: SharedPrefEnum.bearerToken.toString(), value: value);
  }

  void setFirstTimeLogin(bool value) {
    SimpleSharedPref()
        .setBool(key: SharedPrefEnum.firstTimeLogin.toString(), value: value);
  }

  void setActive(bool value) {
    SimpleSharedPref()
        .setBool(key: SharedPrefEnum.active.toString(), value: value);
  }

  void setCustomerId(String value) {
    SimpleSharedPref()
        .setString(key: SharedPrefEnum.customerId.toString(), value: value);
  }

  void setBranchId(String value) {
    SimpleSharedPref()
        .setString(key: SharedPrefEnum.BranchId.toString(), value: value);
  }
}
