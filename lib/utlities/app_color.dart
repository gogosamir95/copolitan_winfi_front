import 'package:flutter/material.dart';

const primaryColor = Color.fromRGBO(255, 255, 255, 1);
const lightPrimaryColor = Color.fromRGBO(250, 250, 250, 0.5);
const primary00Color = Color.fromRGBO(240, 240, 242, 0.0);
// const primaryColor = Color.fromRGBO(240, 240, 242, 1);
// const primaryColor = Color.fromRGBO(240, 240, 242, 1);

/// accent blue colors 11, 0, 172
const accentColor = Color.fromRGBO(12, 11, 174, 1);
const accent90PercentColor = Color.fromRGBO(12, 11, 174, 0.9);
const accent80PercentColor = Color.fromRGBO(12, 11, 174, 0.8);
const accent70PercentColor = Color.fromRGBO(12, 11, 174, 0.7);
const accent60PercentColor = Color.fromRGBO(12, 11, 174, 0.6);
const accent50PercentColor = Color.fromRGBO(12, 11, 174, 0.5);
const accent40PercentColor = Color.fromRGBO(12, 11, 174, 0.4);
const accent30PercentColor = Color.fromRGBO(12, 11, 174, 0.3);
const accent20PercentColor = Color.fromRGBO(12, 11, 174, 0.2);
const accent10PercentColor = Color.fromRGBO(12, 11, 174, 0.1);

/// accent blue colors 11, 0, 172
const winfiAccentColor = Color.fromRGBO(25, 129, 127, 1);
const winfiAccent90PercentColor = Color.fromRGBO(25, 129, 127, 0.9);
const winfiAccent80PercentColor = Color.fromRGBO(25, 129, 127, 0.8);
const winfiAccent70PercentColor = Color.fromRGBO(25, 129, 127, 0.7);
const winfiAccent60PercentColor = Color.fromRGBO(25, 129, 127, 0.6);
const winfiAccent50PercentColor = Color.fromRGBO(25, 129, 127, 0.5);
const winfiAccent40PercentColor = Color.fromRGBO(25, 129, 127, 0.4);
const winfiAccent30PercentColor = Color.fromRGBO(25, 129, 127, 0.3);
const winfiAccent20PercentColor = Color.fromRGBO(25, 129, 127, 0.2);
const winfiAccent10PercentColor = Color.fromRGBO(25, 129, 127, 0.1);

/// white color
const whiteColor = Colors.white;
const whiteColor68Percent = Color.fromRGBO(255, 255, 255, 0.68);
const whiteColor70Percent = Color.fromRGBO(255, 255, 255, 0.7);
const whiteColor69Percent = Color.fromRGBO(255, 255, 255, 0.69);

/// dark white color 227, 227, 227,
const darkerWhiteColor = Color.fromRGBO(227, 227, 227, 1);
const darkWhiteColor27Percent = Color.fromRGBO(227, 227, 227, 0.23);

/// black color
const blackColor = Colors.black;
const blackColor50Percent = Color.fromRGBO(0, 0, 0, 0.5);
const blackColor70Percent = Color.fromRGBO(0, 0, 0, 0.7);
const blackColor75Percent = Color.fromRGBO(0, 0, 0, 0.75);
const blackColor80Percent = Color.fromRGBO(0, 0, 0, 0.8);
const blackColor19Percent = Color.fromRGBO(0, 0, 0, 0.19);
const blackColor16Percent = Color.fromRGBO(0, 0, 0, 0.16);
const blackColor15Percent = Color.fromRGBO(0, 0, 0, 0.15);
const blackColor14Percent = Color.fromRGBO(0, 0, 0, 0.14);
const blackColor10Percent = Color.fromRGBO(0, 0, 0, 0.01);

/// gery color 137, 134, 134
const greyColor = Color.fromRGBO(75, 75, 75, 1.0);
const greyColor80Percent = Color.fromRGBO(137, 134, 134, 0.8);
const greyColor70Percent = Color.fromRGBO(137, 134, 134, 0.7);
const greyColor30Percent = Color.fromRGBO(137, 134, 134, 0.3);
const greyColor50Percent = Color.fromRGBO(137, 134, 134, 0.5);
const greyColor10Percent = Color.fromRGBO(137, 134, 134, 0.1);

/// light grey color 112, 112, 112
const lightGreyColor10Percent = Color.fromRGBO(112, 112, 112, 0.1);
const lightGreyColor30Percent = Color.fromRGBO(112, 112, 112, 0.15);
const lightGreyColor9Percent = Color.fromRGBO(112, 112, 112, 0.09);
const lightGreyColor23Percent = Color.fromRGBO(112, 112, 112, 0.23);

/// purple color
const purpleColor = Color.fromRGBO(120, 113, 224, 1);

/// other colors
const lightGreyColorHour = Color.fromRGBO(213, 213, 214, 1.0);
const editProfileCancelButtonColor = Color.fromRGBO(246, 78, 96, 1);
const darkWhiteColor = Color.fromRGBO(249, 249, 249, 1.0);
const redColor = Color.fromRGBO(243, 57, 1, 1);
const Color TextFormFiledBorderColor = Color.fromRGBO(187, 187, 187, 1);
