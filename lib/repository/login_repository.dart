import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/models/login/login_mapper.dart';
import 'package:copolitan_winfi/models/login/login_request.dart';
import 'package:copolitan_winfi/models/login/login_response.dart';
import 'package:copolitan_winfi/network/api_client.dart';
import 'package:copolitan_winfi/network/dio_helper.dart';
import 'package:copolitan_winfi/repository/send_sms_repository.dart';

class LoginRepository {
  Stream<ApiState<LoginMapper>> login({required String mobile}) async* {
    yield LoadingState();
    try {
      final response = await ApiClient(DioHelper().getDio()).login(
          LoginRequest(mobile.startsWith('0') ? '2$mobile' : '20$mobile'));
      if (response.success! && response.data != null) {
        var loginMapper = LoginMapper(LoginResponse.fromJson(response.data));
        final sendSmsResponse = await SendSmsRepository()
            .sendSms(mobile: mobile, code: loginMapper.otp);
        if (sendSmsResponse is SuccessState)
          yield SuccessState(loginMapper);
        else
          yield FailedState(
              message: sendSmsResponse.message,
              loggerName: sendSmsResponse.loggerName);
      } else
        yield FailedState(
            message: response.message ?? '', loggerName: 'login api failed:');
    } catch (e) {
      yield ErrorState(message: e.toString(), loggerName: 'login api error: ');
    }
  }
}
