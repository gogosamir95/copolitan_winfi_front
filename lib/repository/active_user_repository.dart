import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/repository/verify_user_repository.dart';

class ActiveUserRepository {
  Stream<ApiState> activeUser(
      {required String userName,
      required String password,
      required String mac,
      required String userId,
      required BuildContext context}) async* {
    yield LoadingState();
    try {
      final verifyUser =
          await VerifyUserRepository().verifyUser(mac: mac, id: userId);
      if (verifyUser is SuccessState) {
        // final response = await InfoClient(DioHelper().getDio())
        //     .activeInternet(ActiveUserRequest(userName, password));
        // if (response.statusCode == 200)
        // ViewHelper(context).launchURL('https://wifi.winfico.com/login?username=$userName&password=$password');
        yield SuccessState(null);
        // else
        //   yield FailedState(
        //       message: response.statusMessage ?? '',
        //       loggerName: 'active user api failed');
      } else
        yield ErrorState(
            message: verifyUser.message, loggerName: runtimeType.toString());
    } catch (e) {
      yield ErrorState(
          message: e.toString(), loggerName: runtimeType.toString());
    }
  }
}
