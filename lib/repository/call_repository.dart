import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/models/call/call_request.dart';
import 'package:copolitan_winfi/models/call/voice_request.dart';
import 'package:copolitan_winfi/network/infoBip/info_bip_client.dart';
import 'package:copolitan_winfi/network/infoBip/info_bip_dio_helper.dart';

class CallRepository {
  Future<ApiState> doCall({required String otp, required String mobile}) async {
    try {
      final response = await InfoBipClient(InfoBipDioHelper().getDio())
          .sendCall(CallRequest(
              text:
                  'Hello, your winfi code is,, ${otp[0]},, ${otp[1]},, ${otp[2]},'
                  ', ${otp[3]},, ${otp[4]},, ${otp[5]},, again, your winfi c'
                  'ode is, ${otp[0]}, ${otp[1]}, ${otp[2]}, ${otp[3]}, ${otp[4]}, ${otp[5]}',
              to: mobile.startsWith('0') ? '2$mobile' : '20$mobile',
              from: '',
              language: 'en',
              pause: 20,
              speechRate: 5,
              voiceRequest: VoiceRequest(
                name: 'Joanna',
                gender: 'female',
              )));
      if (response.bulkId != null)
        return SuccessState(null);
      else
        return FailedState(
            message: 'call api failed: ', loggerName: 'call api failed: ');
    } catch (e) {
      return ErrorState(message: e.toString(), loggerName: 'call api error: ');
    }
  }
}
