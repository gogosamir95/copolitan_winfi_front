import 'dart:convert';

import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/models/sendSMS/request/destination_request.dart';
import 'package:copolitan_winfi/models/sendSMS/request/message_request.dart';
import 'package:copolitan_winfi/models/sendSMS/request/send_sms_request.dart';
import 'package:copolitan_winfi/network/infoBip/info_bip_client.dart';
import 'package:copolitan_winfi/network/infoBip/info_bip_dio_helper.dart';
import 'package:copolitan_winfi/utlities/logger.dart';

class SendSmsRepository {
  Future<ApiState> sendSms(
      {required String mobile, required String code}) async {
    try {
      var message = 'your code is: $code';
      final List<MessageRequest> list = [];
      list.add(MessageRequest(
          message: message,
          DestinationList: [
            DestinationRequest(
                to: mobile.startsWith('0') ? '2$mobile' : '20$mobile')
          ],
          from: 'WinFiCo'));
      Logger.log(message: json.encode(list), name: 'message list:');
      final response = await InfoBipClient(InfoBipDioHelper().getDio())
          .sms(SendSmsRequest(messageList: list));
      if (response.messageList != null)
        return SuccessState(null);
      else
        return FailedState(
            message: 'send sms api failed: ',
            loggerName: 'send sms api failed: ');
    } catch (e) {
      return ErrorState(message: e.toString(), loggerName: 'send sms error: ');
    }
  }
}
