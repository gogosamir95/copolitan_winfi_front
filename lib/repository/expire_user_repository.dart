import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/models/verifyUser/verify_user_request.dart';
import 'package:copolitan_winfi/network/api_client.dart';
import 'package:copolitan_winfi/network/dio_helper.dart';

class ExpireUserRepository {
  Stream<ApiState> expireUser(
      {required String mac, required String id}) async* {
    yield LoadingState();
    try {
      final response = await ApiClient(DioHelper().getDio())
          .expireUser(VerifyUserRequest(mac, id));
      if (response.success!)
        yield SuccessState(null);
      else
        yield FailedState(
            message: response.message ?? '',
            loggerName: runtimeType.toString());
    } catch (e) {
      yield ErrorState(
          message: e.toString(), loggerName: runtimeType.toString());
    }
  }
}
