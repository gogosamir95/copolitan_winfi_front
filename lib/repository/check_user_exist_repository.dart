import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/models/checkUserExist/check_user_exist_request.dart';
import 'package:copolitan_winfi/models/login/login_mapper.dart';
import 'package:copolitan_winfi/models/login/login_response.dart';
import 'package:copolitan_winfi/network/api_client.dart';
import 'package:copolitan_winfi/network/dio_helper.dart';

class CheckUserExistRepository {
  Future<ApiState<LoginMapper>> getUserMac({required String mac}) async {
    try {
      final response = await ApiClient(DioHelper().getDio())
          .checkMacExist(CheckUserExistRequest(mac));
      if (response.success!)
        return SuccessState(LoginMapper(LoginResponse.fromJson(response.data)));
      else
        return FailedState(
            message: response.message ?? '',
            loggerName: runtimeType.toString());
    } catch (e) {
      return FailedState(
          message: e.toString(), loggerName: runtimeType.toString());
    }
  }
}
