import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/models/verifyUser/verify_user_request.dart';
import 'package:copolitan_winfi/network/api_client.dart';
import 'package:copolitan_winfi/network/dio_helper.dart';

class VerifyUserRepository {
  Future<ApiState> verifyUser({required String mac, required String id}) async {
    try {
      final response = await ApiClient(DioHelper().getDio())
          .verifyUserWithMacAddress(VerifyUserRequest(mac, id));
      if (response.success!)
        return SuccessState(null);
      else
        return FailedState(
            message: response.message ?? '',
            loggerName: runtimeType.toString());
    } catch (e) {
      return ErrorState(
          message: e.toString(), loggerName: runtimeType.toString());
    }
  }
}
