import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/screens/login/login_screen.dart';
import 'package:copolitan_winfi/screens/verification/verify_bloc.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/utlities/view_helper.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';

class PleaseEnterVerificationCodeWidget extends StatelessWidget {
  final double textSize;
  final String mobileNumber;
  final VerifyBloc bloc;

  const PleaseEnterVerificationCodeWidget(
      {Key? key,
      required this.textSize,
      required this.mobileNumber,
      required this.bloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            child: CustomText(
              text:
                  S.of(context).pleaseEnterVerificationCodeSentTo(mobileNumber),
              customTextStyle:
                  RegularStyle(fontSize: textSize, color: blackColor),
              softWrap: true,
            ),
            fit: FlexFit.loose,
          ),
          SizedBox(
            width: 5,
          ),
          InkWell(
            onTap: () {
              ViewHelper(context).pushScreen(
                  LoginScreen(
                    mac: bloc.mac,
                  ),
                  hasBackStack: true);
            },
            child: CustomText(
                text: S.of(context).edit,
                customTextStyle: SemiBoldStyle(
                    fontSize: textSize + 2,
                    color: accentColor,
                    textDecoration: TextDecoration.underline)),
          ),
        ],
      );
}
