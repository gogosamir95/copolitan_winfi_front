import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/base/response_handler.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/utlities/image_const.dart';
import 'package:copolitan_winfi/utlities/view_helper.dart';
import 'package:copolitan_winfi/widgets/custom_progress.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';
import 'package:image_loader/image_helper.dart';

import '../verify_bloc.dart';

class CallAndSmsWidget extends StatelessWidget with ResponseHandLer {
  final VerifyBloc bloc;
  final MainAxisAlignment mainAxisAlignment;

  const CallAndSmsWidget({
    Key? key,
    required this.bloc,
    this.mainAxisAlignment = MainAxisAlignment.start,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: mainAxisAlignment,
        children: [
          getAutomatedCallButton(context),
          SizedBox(
            width: 8,
          ),
          getOrText(context),
          SizedBox(
            width: 8,
          ),
          getSmsButton(context)
        ],
      );

  Widget getAutomatedCallButton(BuildContext context) => InkWell(
        onTap: () {
          doCall(context);
        },
        child: AnimatedContainer(
          duration: Duration(milliseconds: 300),
          curve: Curves.fastOutSlowIn,
          padding: EdgeInsets.symmetric(horizontal: 14, vertical: 12),
          decoration: BoxDecoration(
            color: accentColor,
            borderRadius: BorderRadius.circular(50),
            boxShadow: [
              BoxShadow(
                  color: accent50PercentColor,
                  offset: Offset(0, 2),
                  blurRadius: 24,
                  spreadRadius: 0)
            ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ImageHelper(
                image: automatedCallIcon,
                imageType: ImageType.svg,
                color: whiteColor,
                boxFit: BoxFit.contain,
                width: 30,
                height: 30,
              ),
              SizedBox(
                width: 12,
              ),
              CustomText(
                  text: S.of(context).automatedCall,
                  customTextStyle:
                      SemiBoldStyle(color: whiteColor, fontSize: 10)),
            ],
          ),
        ),
      );

  Widget getOrText(BuildContext context) => CustomText(
      text: S.of(context).or,
      customTextStyle: RegularStyle(color: blackColor, fontSize: 16));

  Widget getSmsButton(BuildContext context) => InkWell(
        onTap: () {
          sendSms(context);
        },
        child: AnimatedContainer(
          duration: Duration(milliseconds: 300),
          curve: Curves.fastOutSlowIn,
          padding: EdgeInsets.symmetric(horizontal: 14, vertical: 12),
          decoration: BoxDecoration(
            color: whiteColor,
            border: Border.all(color: accentColor, width: 1),
            borderRadius: BorderRadius.circular(50),
          ),
          child: Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                ImageHelper(
                  image: smsIcon,
                  color: accentColor,
                  imageType: ImageType.svg,
                  width: 25,
                  height: 25,
                ),
                SizedBox(
                  width: 12,
                ),
                CustomText(
                    text: S.of(context).sms,
                    customTextStyle:
                        RegularStyle(color: blackColor, fontSize: 10)),
              ],
            ),
          ),
        ),
      );

  void sendSms(BuildContext context) {
    ViewHelper(context).showDialogWithAnimation(CustomProgress());
    bloc.sendSmsAgain.then((value) {
      Navigator.pop(context);
      ViewHelper(context)
          .showSnakeBar(S.of(context).messageSentAgain(bloc.mobile));
    });
  }

  void doCall(BuildContext context) {
    ViewHelper(context).showDialogWithAnimation(CustomProgress());
    bloc.call.then((value) {
      Navigator.pop(context);
      ViewHelper(context)
          .showSnakeBar(S.of(context).messageSentAgain(bloc.mobile));
    });
  }
}
