import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';

class DidNotReceiveCodeWidget extends StatelessWidget {
  final double textSize;

  const DidNotReceiveCodeWidget({Key? key, this.textSize = 16})
      : super(key: key);

  @override
  Widget build(BuildContext context) => CustomText(
      text: S.of(context).didNotReceiveCodeYet,
      customTextStyle:
          RegularStyle(color: blackColor80Percent, fontSize: textSize));
}
