import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';

class VerificationCodeWidget extends StatelessWidget {
  final CustomTextStyle customTextStyle;

  const VerificationCodeWidget({Key? key, required this.customTextStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) => CustomText(
      text: S.of(context).verificationCode, customTextStyle: customTextStyle);
}
