import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/base/response_handler.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/screens/verification/verify_bloc.dart';
import 'package:copolitan_winfi/widgets/custom_button.dart';
import 'package:url_launcher/url_launcher.dart';

class VerifyButtonWidget extends StatelessWidget with ResponseHandLer {
  final VerifyBloc bloc;

  const VerifyButtonWidget({Key? key, required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) => _getButton(context);

  Widget _getButton(BuildContext context) => CustomButton(
        idleText: S.of(context).verifyAndConnect,
        onTap: () {
          verify(context);
        },
        validateStream: bloc.validateStream,
        failedBehaviour: bloc.failedBehaviour,
        buttonBehaviour: bloc.buttonBehavior,
      );

  void verify(BuildContext context) {
    if (bloc.isValid) {
      bloc.getActiveUserInternet(context).listen((event) {
        checkResponseStateForButton(event, context,
            failedBehaviour: bloc.failedBehaviour,
            buttonBehaviour: bloc.buttonBehavior, onSuccess: () async {
          await launch(
            'https://wifi.winfico.com/login?username=${bloc.mobile}&password=${bloc.password}',
            forceSafariVC: true,
            forceWebView: true,
            webOnlyWindowName: '_self',
          );
        });
      });
    }
  }
}
