import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/screens/verification/verify_bloc.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:flutter/services.dart';
import 'package:form_field_validator/form_field_validator.dart';
import 'package:pin_input_text_field/pin_input_text_field.dart';

class PinCodeWidget extends StatelessWidget {
  final VerifyBloc bloc;

  const PinCodeWidget({Key? key, required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) => _pinCode;

  Widget get _pinCode => Directionality(
        textDirection: TextDirection.ltr,
        child: Container(
            child: StreamBuilder(
                stream: bloc.otpStream,
                builder: (context, snapshot) => PinInputTextFormField(
                  validator: (value) =>
                          MatchValidator(errorText: S.of(context).invalidOTP)
                              .validateMatch(value ?? '', bloc.otp),
                      textInputAction: TextInputAction.done,
                      pinLength: bloc.pinLength,
                      cursor: Cursor(
                          color: accentColor,
                          enabled: true,
                          height: 20,
                          width: 1),
                      inputFormatters: [
                        FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                      ],
                      onChanged: (value) {
                        _hideKeyboard(context);
                        bloc.updateOtp(value);
                      },
                      decoration: BoxLooseDecoration(
                        textStyle: BoldStyle(color: accentColor, fontSize: 27)
                            .getStyle(),
                        gapSpace: MediaQuery.of(context).size.width * 0.02,
                        radius: Radius.circular(5),
                        strokeColorBuilder: PinListenColorBuilder(
                            blackColor80Percent, greyColor),
                        bgColorBuilder:
                            PinListenColorBuilder(whiteColor, whiteColor),
                        errorTextStyle:
                            RegularStyle(color: redColor, fontSize: 18)
                                .getStyle(),
                        strokeWidth: 1,
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                    ))),
      );

  void _hideKeyboard(BuildContext context) {
    // if (Platform.isIOS || Platform.isAndroid)
    //   FocusScope.of(context).requestFocus(FocusNode());
  }
}
