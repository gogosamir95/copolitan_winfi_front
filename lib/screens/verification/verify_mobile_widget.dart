import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/call_sms_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/did_not_receive_code_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/pin_code_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/please_enter_verification_code_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/resend_va_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/verification_code_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/verify_button_widget.dart';
import 'package:copolitan_winfi/screens/verification/verify_bloc.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/copolitan_logo_widget.dart';
import 'package:copolitan_winfi/widgets/copolitan_text_widget.dart';
import 'package:copolitan_winfi/widgets/powered_by_winfi_widget.dart';

class VerifyMobileWidget extends StatelessWidget {
  final VerifyBloc bloc;

  const VerifyMobileWidget({
    Key? key,
    required this.bloc,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: [
              SizedBox(
                height: 40,
              ),
              Center(
                child: CopolitanLogoWidget(),
              ),
              SizedBox(
                height: 20,
              ),
              Center(
                child: CopolitanTextWidget(
                    customTextStyle:
                        BoldStyle(color: blackColor, fontSize: 18)),
              ),
              SizedBox(
                height: 20,
              ),
              VerificationCodeWidget(
                  customTextStyle: BoldStyle(color: blackColor, fontSize: 18)),
              PleaseEnterVerificationCodeWidget(
                textSize: 16,
                mobileNumber: bloc.mobile,
                bloc: bloc,
              ),
              SizedBox(
                height: 25,
              ),
              Center(
                  child: PinCodeWidget(
                bloc: bloc,
              )),
              SizedBox(
                height: 25,
              ),
              Center(
                child: VerifyButtonWidget(
                  bloc: bloc,
                ),
              ),
              SizedBox(
                height: 25,
              ),
              DidNotReceiveCodeWidget(),
              ResendVaWidget(),
              SizedBox(
                height: 12,
              ),
              Center(
                  child: CallAndSmsWidget(
                      bloc: bloc, mainAxisAlignment: MainAxisAlignment.center)),
              SizedBox(
                height: 20,
              ),
              PoweredByWinfi(
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            ],
          ),
        ),
      );
}
