import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/call_sms_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/did_not_receive_code_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/pin_code_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/please_enter_verification_code_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/resend_va_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/verification_code_widget.dart';
import 'package:copolitan_winfi/screens/verification/verificationWidget/verify_button_widget.dart';
import 'package:copolitan_winfi/screens/verification/verify_bloc.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/web_left_column_widget.dart';

class VerifyWebWidget extends StatelessWidget {
  final VerifyBloc bloc;

  const VerifyWebWidget({Key? key, required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        children: [
          WebLeftColumnWidget(),
          Spacer(),
          rightColumn,
          Spacer(),
        ],
      );

  Widget get rightColumn => Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            VerificationCodeWidget(
                customTextStyle: BoldStyle(color: blackColor, fontSize: 27)),
            PleaseEnterVerificationCodeWidget(
              textSize: 16,
              mobileNumber: bloc.mobile,
              bloc: bloc,
            ),
            SizedBox(
              height: 25,
            ),
            Center(
                child: PinCodeWidget(
              bloc: bloc,
            )),
            SizedBox(
              height: 25,
            ),
            Center(
              child: VerifyButtonWidget(
                bloc: bloc,
              ),
            ),
            SizedBox(
              height: 25,
            ),
            DidNotReceiveCodeWidget(),
            ResendVaWidget(),
            SizedBox(
              height: 12,
            ),
            CallAndSmsWidget(bloc: bloc)
          ],
        ),
        flex: 5,
      );
}
