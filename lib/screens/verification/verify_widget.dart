import 'dart:async';

import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/base/bloc_base.dart';
import 'package:copolitan_winfi/screens/verification/verify_bloc.dart';
import 'package:copolitan_winfi/screens/verification/verify_mobile_widget.dart';
import 'package:copolitan_winfi/screens/verification/verify_web_widget.dart';
import 'package:copolitan_winfi/widgets/responsive.dart';

class VerifyWidget extends BaseStatefulWidget {
  final String mobile;
  final String otp;
  final String password;
  final String id;
  final String mac;

  const VerifyWidget(
      {Key? key,
      required this.mobile,
      required this.otp,
      required this.password,
      required this.mac,
      required this.id})
      : super(key: key);

  @override
  _VerifyWidgetState createState() => _VerifyWidgetState();
}

class _VerifyWidgetState extends BaseState<VerifyWidget> {
  final VerifyBloc _bloc = VerifyBloc();

  @override
  void initState() {
    super.initState();
    _bloc.init();
    _bloc.mobile = widget.mobile;
    _bloc.otp = widget.otp;
    _bloc.password = widget.password;
    _bloc.mac = widget.mac;
    _bloc.id = widget.id;
  }

  @override
  bool isSafeArea() => true;

  @override
  Future<bool> willPopBack() async => false;

  @override
  PreferredSizeWidget? appBar() => null;

  @override
  Widget getBody(BuildContext context) => blocProvider;

  BlocProvider get blocProvider =>
      BlocProvider(child: _screenDesign, bloc: _bloc);

  Widget get _screenDesign => Responsive(
      mobile: VerifyMobileWidget(
        bloc: _bloc,
      ),
      tablet: VerifyWebWidget(
        bloc: _bloc,
      ),
      web: VerifyWebWidget(
        bloc: _bloc,
      ));
}
