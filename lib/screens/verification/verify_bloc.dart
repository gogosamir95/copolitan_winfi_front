import 'dart:async';

import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/base/button_bloc.dart';
import 'package:copolitan_winfi/base/validator.dart';
import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/repository/active_user_repository.dart';
import 'package:copolitan_winfi/repository/call_repository.dart';
import 'package:copolitan_winfi/repository/send_sms_repository.dart';
import 'package:rxdart/rxdart.dart';
import 'package:timer_count_down/timer_controller.dart';

class VerifyBloc extends ButtonBloc {
  final _otpBehaviour = BehaviorSubject<String>();
  final isResendCodeActiveBehaviour = BehaviorSubject<bool>();
  final isAutomatedCallBehaviour = BehaviorSubject<bool>();
  final int pinLength = 6;
  late final CountdownController smsController, automatedCallController;
  final int sMSTimer = 180, autoMatedCallTimer = 180;
  var _mobile, _otp, _password, _id, _mac;

  @override
  void init() {
    super.init();
    isResendCodeActiveBehaviour.sink.add(false);
    smsController = CountdownController(autoStart: true);
    automatedCallController = CountdownController(autoStart: false);
  }

  String get inputOTP => _otpBehaviour.value;

  Stream<String> get otpStream => _otpBehaviour.stream;

  Function(String) get updateOtp => _otpBehaviour.sink.add;

  Stream<ApiState> getActiveUserInternet(BuildContext context) =>
      ActiveUserRepository().activeUser(
          userName: mobile.startsWith('0') ? '2$mobile' : '20$mobile',
          password: password,
          mac: mac,
          userId: id,
          context: context);

  Stream<bool> get validateStream =>
      Rx.combineLatest([otpStream], (List<String> combiners) {
        if (isValid) return true;
        return false;
      });

  Future<ApiState> get sendSmsAgain =>
      SendSmsRepository().sendSms(mobile: mobile, code: otp);

  Future<ApiState> get call =>
      CallRepository().doCall(mobile: mobile, otp: otp);

  set mobile(String value) => _mobile = value;

  String get mobile => _mobile;

  bool get isValid =>
      Validator().isOtpValid(_otpBehaviour.value) && _otpBehaviour.value == otp;

  String get otp => _otp;

  set otp(String value) => _otp = value;

  String get password => _password;

  set password(String value) {
    _password = value;
  }

  String get mac => _mac;

  set mac(String value) {
    _mac = value;
  }

  String get id => _id;

  set id(String value) {
    _id = value;
  }

  @override
  void dispose() {
    super.dispose();
    _otpBehaviour.close();
    isResendCodeActiveBehaviour.close();
  }
}
