import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/base/bloc_base.dart';
import 'package:copolitan_winfi/screens/welcomeBack/welcome_back_bloc.dart';
import 'package:copolitan_winfi/screens/welcomeBack/welcome_back_mobile_widget.dart';
import 'package:copolitan_winfi/screens/welcomeBack/welcome_back_web_widget.dart';
import 'package:copolitan_winfi/widgets/responsive.dart';

class WelcomeBackWidget extends BaseStatefulWidget {
  final int userId;
  final String userName;
  final String mobile;
  final String password;
  final String macAddress;

  const WelcomeBackWidget(
      {Key? key,
      required this.userId,
      required this.userName,
      required this.mobile,
      required this.password,
      required this.macAddress})
      : super(key: key);

  @override
  _WelcomeBackWidgetState createState() => _WelcomeBackWidgetState();
}

class _WelcomeBackWidgetState extends BaseState<WelcomeBackWidget> {
  final WelcomeBackBloc _bloc = WelcomeBackBloc();

  @override
  void initState() {
    _initBloc();
    super.initState();
  }

  void _initBloc() {
    _bloc.init();
    _bloc.userName = widget.userName;
    _bloc.id = widget.userId;
    _bloc.mobile = widget.mobile;
    _bloc.password = widget.password;
    _bloc.macAddress = widget.macAddress;
  }

  @override
  bool isSafeArea() => true;

  @override
  Future<bool> willPopBack() async => false;

  @override
  PreferredSizeWidget? appBar() => null;

  BlocProvider get _blocProvider =>
      BlocProvider(child: _screenDesign, bloc: _bloc);

  Widget get _screenDesign => Responsive(
      mobile: WelcomeBackMobileWidget(
        bloc: _bloc,
      ),
      tablet: WelcomeBackWebWidget(
        bloc: _bloc,
      ),
      web: WelcomeBackWebWidget(
        bloc: _bloc,
      ));

  @override
  Widget getBody(BuildContext context) => _blocProvider;
}
