import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';

class UserNameWidget extends StatelessWidget {
  final double textSize;
  final String userName;

  const UserNameWidget(
      {Key? key, required this.userName, required this.textSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) => CustomText(
      text: userName,
      customTextStyle:
          SemiBoldStyle(fontSize: textSize, color: blackColor80Percent));
}
