import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';

class WelcomeBackTextWidget extends StatelessWidget {
  final double textSize;

  const WelcomeBackTextWidget({Key? key, required this.textSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) => CustomText(
      text: S.of(context).welcomeBack,
      customTextStyle:
          RegularStyle(fontSize: textSize, color: blackColor80Percent));
}
