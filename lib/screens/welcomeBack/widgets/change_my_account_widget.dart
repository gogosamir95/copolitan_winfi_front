import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/screens/login/login_screen.dart';
import 'package:copolitan_winfi/screens/welcomeBack/welcome_back_bloc.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/utlities/view_helper.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';

class ChangeMyAccountWidget extends StatelessWidget {
  final double textSize;
  final WelcomeBackBloc bloc;

  const ChangeMyAccountWidget(
      {Key? key, required this.textSize, required this.bloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: () {
          // ViewHelper(context).initProgressDialog();
          // ViewHelper(context).showProgressDialog();
          bloc.expireUser.listen((event) {
            if (event is SuccessState) {
              // ViewHelper(context).hideProgress();
              ViewHelper(context).pushScreen(LoginScreen(mac: bloc.macAddress),
                  hasBackStack: true);
            }
          });
        },
        child: CustomText(
            text: S.of(context).changeMyAccount,
            customTextStyle: RegularStyle(
              color: accentColor,
              fontSize: 18,
            )),
      );
}
