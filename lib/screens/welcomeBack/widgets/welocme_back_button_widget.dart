import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/base/response_handler.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/screens/welcomeBack/welcome_back_bloc.dart';
import 'package:copolitan_winfi/widgets/custom_button.dart';
import 'package:url_launcher/url_launcher.dart';

class WelcomeBackButtonWidget extends StatelessWidget with ResponseHandLer {
  final WelcomeBackBloc bloc;

  const WelcomeBackButtonWidget({Key? key, required this.bloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) => _getButton(context);

  Widget _getButton(BuildContext context) => CustomButton(
        idleText: S.of(context).connectToInternet,
        onTap: () {
          connectToInternet(context);
        },
        failedBehaviour: bloc.failedBehaviour,
        buttonBehaviour: bloc.buttonBehavior,
      );

  void connectToInternet(BuildContext context) {
    bloc.getActiveUserInternet(context).listen((event) {
      checkResponseStateForButton(event, context,
          failedBehaviour: bloc.failedBehaviour,
          buttonBehaviour: bloc.buttonBehavior, onSuccess: () async {
        await launch(
          'https://wifi.winfico.com/login?username=${bloc.mobile}&password=${bloc.password}',
          forceSafariVC: true,
          forceWebView: true,
          webOnlyWindowName: '_self',
        );
      });
    });
  }
}
