import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/base/button_bloc.dart';
import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/repository/active_user_repository.dart';
import 'package:copolitan_winfi/repository/expire_user_repository.dart';

class WelcomeBackBloc extends ButtonBloc {
  var _id, _userName, _mobile, _password, _macAddress;

  int get id => _id;

  set id(int value) {
    _id = value;
  }

  String get userName => _userName;

  set userName(String value) {
    _userName = value;
  }

  String get mobile => _mobile;

  set mobile(String value) {
    _mobile = value;
  }

  String get password => _password;

  set password(String value) {
    _password = value;
  }

  String get macAddress => _macAddress;

  set macAddress(String value) {
    _macAddress = value;
  }

  Stream<ApiState> getActiveUserInternet(BuildContext context) =>
      ActiveUserRepository().activeUser(
          userName: mobile.startsWith('0') ? '2$mobile' : '20$mobile',
          password: password,
          userId: id.toString(),
          mac: macAddress,
          context: context);

  Stream<ApiState> get expireUser =>
      ExpireUserRepository().expireUser(mac: macAddress, id: id.toString());

  @override
  void init() {
    super.init();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
