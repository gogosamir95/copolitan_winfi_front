import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/screens/welcomeBack/welcome_back_bloc.dart';
import 'package:copolitan_winfi/screens/welcomeBack/widgets/change_my_account_widget.dart';
import 'package:copolitan_winfi/screens/welcomeBack/widgets/user_name_widget.dart';
import 'package:copolitan_winfi/screens/welcomeBack/widgets/welcome_back_text_widget.dart';
import 'package:copolitan_winfi/screens/welcomeBack/widgets/welocme_back_button_widget.dart';
import 'package:copolitan_winfi/widgets/web_left_column_widget.dart';

class WelcomeBackWebWidget extends StatelessWidget {
  final WelcomeBackBloc bloc;

  const WelcomeBackWebWidget({Key? key, required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        children: [
          WebLeftColumnWidget(),
          Spacer(),
          getRightColumn(context),
          Spacer(),
        ],
      );

  Widget getRightColumn(BuildContext context) => Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            WelcomeBackTextWidget(textSize: 32),
            SizedBox(
              height: 5,
            ),
            UserNameWidget(userName: bloc.userName, textSize: 36),
            SizedBox(
              height: 32,
            ),
            WelcomeBackButtonWidget(bloc: bloc),
            SizedBox(
              height: 17,
            ),
            ChangeMyAccountWidget(
              textSize: 18,
              bloc: bloc,
            )
          ],
        ),
        flex: 5,
      );

  Widget getAnimatedContainer(
          {required Widget child, required BuildContext context}) =>
      AnimatedContainer(
          duration: Duration(milliseconds: 300),
          alignment: Alignment.centerLeft,
          curve: Curves.fastOutSlowIn,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.05),
            child: child,
          ));
}
