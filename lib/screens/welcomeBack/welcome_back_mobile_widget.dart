import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/screens/welcomeBack/welcome_back_bloc.dart';
import 'package:copolitan_winfi/screens/welcomeBack/widgets/change_my_account_widget.dart';
import 'package:copolitan_winfi/screens/welcomeBack/widgets/user_name_widget.dart';
import 'package:copolitan_winfi/screens/welcomeBack/widgets/welcome_back_text_widget.dart';
import 'package:copolitan_winfi/screens/welcomeBack/widgets/welocme_back_button_widget.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/copolitan_logo_widget.dart';
import 'package:copolitan_winfi/widgets/copolitan_text_widget.dart';
import 'package:copolitan_winfi/widgets/powered_by_winfi_widget.dart';

class WelcomeBackMobileWidget extends StatelessWidget {
  final WelcomeBackBloc bloc;

  const WelcomeBackMobileWidget({Key? key, required this.bloc})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Padding(
        padding: EdgeInsets.symmetric(horizontal: 16),
        child: Center(
          child: ListView(
            shrinkWrap: true,
            children: [
              Center(child: CopolitanLogoWidget()),
              SizedBox(
                height: 20,
              ),
              Center(
                child: CopolitanTextWidget(
                    customTextStyle:
                        BoldStyle(color: blackColor, fontSize: 18)),
              ),
              SizedBox(
                height: 20,
              ),
              Center(child: WelcomeBackTextWidget(textSize: 16)),
              SizedBox(
                height: 5,
              ),
              Center(
                  child: UserNameWidget(userName: bloc.userName, textSize: 18)),
              SizedBox(
                height: 20,
              ),
              Center(child: WelcomeBackButtonWidget(bloc: bloc)),
              SizedBox(
                height: 20,
              ),
              Center(
                  child: ChangeMyAccountWidget(
                textSize: 16,
                bloc: bloc,
              )),
              SizedBox(
                height: 20,
              ),
              PoweredByWinfi(
                mainAxisAlignment: MainAxisAlignment.center,
              ),
            ],
          ),
        ),
      );
}
