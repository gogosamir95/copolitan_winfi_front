import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/screens/login/book_at_copolitan_widget.dart';
import 'package:copolitan_winfi/screens/login/download_app_widget.dart';
import 'package:copolitan_winfi/screens/login/login_button_widget.dart';
import 'package:copolitan_winfi/screens/login/login_to_your_account_widget.dart';
import 'package:copolitan_winfi/screens/login/mobile_widget.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/copolitan_text_widget.dart';
import 'package:copolitan_winfi/widgets/web_left_column_widget.dart';

import 'login_bloc.dart';

class WebLoginScreen extends StatefulWidget {
  final LoginBloc bloc;

  const WebLoginScreen({Key? key, required this.bloc}) : super(key: key);

  @override
  _WebLoginScreenState createState() => _WebLoginScreenState();
}

class _WebLoginScreenState extends State<WebLoginScreen> {
  @override
  Widget build(BuildContext context) => Row(
        children: [
          WebLeftColumnWidget(),
          Spacer(),
          rightColumn,
          Spacer(),
        ],
      );

  Widget get rightColumn => Expanded(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            CopolitanTextWidget(
                customTextStyle: BoldStyle(color: blackColor, fontSize: 27)),
            LoginToYourAccountWidget(),
            SizedBox(
              height: 25,
            ),
            Center(child: MobileWidget(bloc: widget.bloc)),
            SizedBox(
              height: 25,
            ),
            Center(
              child: LoginButtonWidget(
                bloc: widget.bloc,
              ),
            ),
            SizedBox(
              height: 25,
            ),
            Center(child: BookAtCopolitanWidget(textSize: 20)),
            SizedBox(
              height: 25,
            ),
            Center(child: FittedBox(child: DownloadAppWidget(textSize: 18))),
          ],
        ),
        flex: 5,
      );
}
