import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';

class LoginToYourAccountWidget extends StatelessWidget {
  final double textSize;

  const LoginToYourAccountWidget({Key? key, this.textSize = 27})
      : super(key: key);

  @override
  Widget build(BuildContext context) => CustomText(
      text: S.of(context).loginToYourAccount,
      customTextStyle: RegularStyle(color: blackColor, fontSize: textSize));
}
