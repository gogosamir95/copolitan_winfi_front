import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/base/response_handler.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/screens/login/login_bloc.dart';
import 'package:copolitan_winfi/screens/verification/verify_widget.dart';
import 'package:copolitan_winfi/utlities/view_helper.dart';
import 'package:copolitan_winfi/widgets/custom_button.dart';

class LoginButtonWidget extends StatelessWidget with ResponseHandLer {
  final LoginBloc bloc;

  const LoginButtonWidget({Key? key, required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) => _getButton(context);

  Widget _getButton(BuildContext context) => CustomButton(
        idleText: S.of(context).login,
        onTap: () {
          login(context);
        },
        validateStream: bloc.validateStream,
        failedBehaviour: bloc.failedBehaviour,
        buttonBehaviour: bloc.buttonBehavior,
      );

  void login(BuildContext context) {
    if (bloc.validate) {
      bloc.loginStream.listen((event) {
        checkResponseStateForButton(event, context,
            failedBehaviour: bloc.failedBehaviour,
            buttonBehaviour: bloc.buttonBehavior, onSuccess: () {
          showOtpScreen(
            context,
            id: event.response!.id,
            mobile: event.response!.mobile,
            otp: event.response!.otp,
            password: event.response!.password,
          );
        });
      });
    }
  }

  void showOtpScreen(BuildContext context,
      {required String mobile,
      required String otp,
      required String password,
      required int id}) {
    ViewHelper(context).pushScreen(
        VerifyWidget(
            mobile:
                bloc.mobile.startsWith('0') ? bloc.mobile : '0${bloc.mobile}',
            otp: otp,
            id: id.toString(),
            mac: bloc.macAddress,
            password: password),
        hasBackStack: true);
  }
}
