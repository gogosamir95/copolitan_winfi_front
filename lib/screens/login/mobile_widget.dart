import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/base/validator.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/widgets/custom_text_form_filed.dart';
import 'package:copolitan_winfi/widgets/egypt_row_widget.dart';
import 'package:flutter/services.dart';

import 'login_bloc.dart';

class MobileWidget extends StatefulWidget {
  final LoginBloc bloc;

  const MobileWidget({Key? key, required this.bloc}) : super(key: key);

  @override
  State<MobileWidget> createState() => _MobileWidgetState();
}

class _MobileWidgetState extends State<MobileWidget> {
  @override
  Widget build(BuildContext context) => _getMobileStream(context);

  StreamBuilder _getMobileStream(BuildContext context) => StreamBuilder(
        builder: (context, snapshot) => CustomTextFormFiled(
          labelText: S.of(context).mobileNumber,
          onChanged: (value) => widget.bloc.updateMobile(value),
          validator: (value) => Validator()
              .mobileValidator(context)
              .call(value!.startsWith('0') ? value : '0$value'),
          prefixIcon: EgyptRowWidget(),
          inputFormatter: [
            FilteringTextInputFormatter.allow(RegExp('[0-9]')),
          ],
        ),
        stream: widget.bloc.mobileStream,
      );
}
