import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/base/bloc_base.dart';
import 'package:copolitan_winfi/screens/login/mobile_login_screen.dart';
import 'package:copolitan_winfi/screens/login/web_login_screen.dart';
import 'package:copolitan_winfi/widgets/responsive.dart';

import 'login_bloc.dart';

class LoginScreen extends BaseStatefulWidget {
  final String mac;

  const LoginScreen({Key? key, required this.mac}) : super(key: key);

  @override
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends BaseState<LoginScreen> {
  final LoginBloc _bloc = LoginBloc();

  @override
  void initState() {
    super.initState();
    _bloc.init();
    _bloc.macAddress = widget.mac;
  }

  @override
  bool isSafeArea() => true;

  @override
  Future<bool> willPopBack() async => false;

  @override
  PreferredSizeWidget? appBar() => null;

  @override
  Widget getBody(BuildContext context) {
    // ViewHelper(context).showSnakeBar('mac is: ${widget.mac}', durationInMilliSeconds: 4000);
    return blocProvider;
  }

  BlocProvider get blocProvider =>
      BlocProvider(child: _screenDesign, bloc: _bloc);

  Widget get _screenDesign => Responsive(
        mobile: MobileLoginScreen(
          bloc: _bloc,
        ),
        tablet: WebLoginScreen(
          bloc: _bloc,
        ),
        web: WebLoginScreen(
          bloc: _bloc,
        ),
      );
}
