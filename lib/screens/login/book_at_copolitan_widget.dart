import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/utlities/view_helper.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';

class BookAtCopolitanWidget extends StatelessWidget {
  final double textSize;

  const BookAtCopolitanWidget({Key? key, required this.textSize})
      : super(key: key);

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: () {
          ViewHelper(context).launchURL('https://copolitan.com/#/');
        },
        child: CustomText(
          text: S.of(context).bookAtCopolitan,
          customTextStyle: RegularStyle(
              color: Colors.blue,
              fontSize: textSize,
              textDecoration: TextDecoration.underline),
          textAlign: TextAlign.center,
        ),
      );
}
