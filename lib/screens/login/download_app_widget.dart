import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/utlities/view_helper.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';

class DownloadAppWidget extends StatelessWidget {
  final double textSize;
  final MainAxisAlignment mainAxisAlignment;

  const DownloadAppWidget(
      {Key? key,
      required this.textSize,
      this.mainAxisAlignment = MainAxisAlignment.center})
      : super(key: key);

  @override
  Widget build(BuildContext context) => Row(
        mainAxisAlignment: mainAxisAlignment,
        children: [
          CustomText(
            text: S.of(context).downloadApp + " (",
            customTextStyle:
                RegularStyle(color: blackColor, fontSize: textSize),
            textAlign: TextAlign.center,
          ),
          SizedBox(
            width: 2,
          ),
          InkWell(
          onTap: () {
            ViewHelper(context).launchURL(
                'https://apps.apple.com/eg/app/copolitan/id1588043934');
          },
          child: CustomText(
            text: S.of(context).IOS,
            customTextStyle:
                RegularStyle(color: Colors.blue, fontSize: textSize + 2),
            textAlign: TextAlign.center,
          ),
        ),
        CustomText(
          text: " ,  ",
          customTextStyle:
              RegularStyle(color: blackColor, fontSize: textSize + 2),
          textAlign: TextAlign.center,
        ),
        InkWell(
          onTap: () {
            ViewHelper(context).launchURL(
                'https://play.google.com/store/apps/details?id=com.technopolitan.copolitan');
          },
          child: CustomText(
            text: S.of(context).android,
            customTextStyle:
                RegularStyle(color: Colors.blue, fontSize: textSize + 2),
            textAlign: TextAlign.center,
          ),
        ),
        CustomText(
          text: " )",
          customTextStyle: RegularStyle(color: blackColor, fontSize: textSize),
          textAlign: TextAlign.center,
        )
      ],
    );
}
