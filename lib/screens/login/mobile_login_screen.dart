import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/screens/login/login_button_widget.dart';
import 'package:copolitan_winfi/screens/login/login_to_your_account_widget.dart';
import 'package:copolitan_winfi/screens/login/mobile_widget.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/copolitan_logo_widget.dart';
import 'package:copolitan_winfi/widgets/copolitan_text_widget.dart';
import 'package:copolitan_winfi/widgets/powered_by_winfi_widget.dart';

import 'book_at_copolitan_widget.dart';
import 'download_app_widget.dart';
import 'login_bloc.dart';

class MobileLoginScreen extends StatelessWidget {
  final LoginBloc bloc;

  const MobileLoginScreen({Key? key, required this.bloc}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 16),
      child: Center(
        child: ListView(
          shrinkWrap: true,
          children: [
            SizedBox(
              height: 40,
            ),
            Center(
              child: CopolitanLogoWidget(),
            ),
            SizedBox(
              height: 40,
            ),
            Center(
              child: CopolitanTextWidget(
                  customTextStyle: BoldStyle(color: blackColor, fontSize: 18)),
            ),
            SizedBox(
              height: 40,
            ),
            Center(
              child: LoginToYourAccountWidget(
                textSize: 18,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            MobileWidget(bloc: bloc),
            SizedBox(
              height: 40,
            ),
            LoginButtonWidget(bloc: bloc),
            SizedBox(
              height: 40,
            ),
            Center(child: BookAtCopolitanWidget(textSize: 18)),
            SizedBox(
              height: 40,
            ),
            DownloadAppWidget(textSize: 16),
            SizedBox(
              height: 40,
            ),
            PoweredByWinfi(
              mainAxisAlignment: MainAxisAlignment.center,
            ),
            SizedBox(
              height: 40,
            ),
          ],
        ),
      ),
    );
  }
}
