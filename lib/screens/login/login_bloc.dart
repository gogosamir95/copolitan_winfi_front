import 'package:copolitan_winfi/base/button_bloc.dart';
import 'package:copolitan_winfi/base/validator.dart';
import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/models/login/login_mapper.dart';
import 'package:copolitan_winfi/repository/login_repository.dart';
import 'package:rxdart/rxdart.dart';

class LoginBloc extends ButtonBloc {
  final _mobileBehaviour = BehaviorSubject<String>();
  var _macAddress;

  Stream<String> get mobileStream => _mobileBehaviour.stream;

  Function(String) get updateMobile => _mobileBehaviour.sink.add;

  String get mobile => _mobileBehaviour.value;

  Stream<bool> get validateStream =>
      Rx.combineLatest([mobileStream], (values) => validate ? true : false);

  bool get validate => _mobileBehaviour.valueOrNull != null
      ? Validator().isMobileValid(_mobileBehaviour.value.startsWith('0')
          ? _mobileBehaviour.value
          : '0${_mobileBehaviour.value}')
      : false;

  Stream<ApiState<LoginMapper>> get loginStream =>
      LoginRepository().login(mobile: _mobileBehaviour.value);

  String get macAddress => _macAddress;

  set macAddress(String value) => _macAddress = value;

  @override
  void init() async {
    super.init();
  }

  @override
  void dispose() {
    super.dispose();
  }
}
