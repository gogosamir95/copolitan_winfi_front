// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'info_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

InfoResponse _$InfoResponseFromJson(Map<String, dynamic> json) => InfoResponse()
  ..mac = json['mac'] as String?
  ..ip = json['ip'] as String?
  ..linkLogin = json['link-login'] as String?
  ..linkLoginOnly = json['link-login-only'] as String?;

Map<String, dynamic> _$InfoResponseToJson(InfoResponse instance) =>
    <String, dynamic>{
      'mac': instance.mac,
      'ip': instance.ip,
      'link-login': instance.linkLogin,
      'link-login-only': instance.linkLoginOnly,
    };
