import 'package:json_annotation/json_annotation.dart';

part 'info_response.g.dart';

@JsonSerializable(explicitToJson: true)
class InfoResponse {
  @JsonKey(name: 'mac')
  String? mac;
  @JsonKey(name: 'ip')
  String? ip;
  @JsonKey(name: 'link-login')
  String? linkLogin;
  @JsonKey(name: 'link-login-only')
  String? linkLoginOnly;

  Map<String, dynamic> toJson() => _$InfoResponseToJson(this);

  factory InfoResponse.fromJson(Map<String, dynamic> json) =>
      _$InfoResponseFromJson(json);

  InfoResponse();
}
