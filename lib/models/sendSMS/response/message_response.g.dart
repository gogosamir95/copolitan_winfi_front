// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageResponse _$MessageResponseFromJson(Map<String, dynamic> json) =>
    MessageResponse()
      ..to = json['to'] as String?
      ..statusResponse = json['status'] == null
          ? null
          : StatusResponse.fromJson(json['status'] as Map<String, dynamic>)
      ..messageId = json['messageId'] as String?;

Map<String, dynamic> _$MessageResponseToJson(MessageResponse instance) =>
    <String, dynamic>{
      'to': instance.to,
      'status': instance.statusResponse?.toJson(),
      'messageId': instance.messageId,
    };
