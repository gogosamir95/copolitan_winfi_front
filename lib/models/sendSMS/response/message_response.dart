import 'package:copolitan_winfi/models/sendSMS/response/status_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'message_response.g.dart';

@JsonSerializable(explicitToJson: true)
class MessageResponse {
  @JsonKey(name: 'to')
  String? to;
  @JsonKey(name: 'status')
  StatusResponse? statusResponse;
  @JsonKey(name: 'messageId')
  String? messageId;

  MessageResponse();

  Map<String, dynamic> toJson() => _$MessageResponseToJson(this);

  factory MessageResponse.fromJson(Map<String, dynamic> json) =>
      _$MessageResponseFromJson(json);
}
