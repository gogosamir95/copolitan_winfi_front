// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sms_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SmsResponse _$SmsResponseFromJson(Map<String, dynamic> json) => SmsResponse()
  ..messageList = (json['messages'] as List<dynamic>?)
      ?.map((e) => MessageResponse.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$SmsResponseToJson(SmsResponse instance) =>
    <String, dynamic>{
      'messages': instance.messageList?.map((e) => e.toJson()).toList(),
    };
