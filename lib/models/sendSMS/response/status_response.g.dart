// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'status_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

StatusResponse _$StatusResponseFromJson(Map<String, dynamic> json) =>
    StatusResponse()
      ..groupId = json['groupId'] as int?
      ..groupName = json['groupName'] as String?
      ..id = json['id'] as int?
      ..name = json['name'] as String?
      ..description = json['description'] as String?;

Map<String, dynamic> _$StatusResponseToJson(StatusResponse instance) =>
    <String, dynamic>{
      'groupId': instance.groupId,
      'groupName': instance.groupName,
      'id': instance.id,
      'name': instance.name,
      'description': instance.description,
    };
