import 'package:json_annotation/json_annotation.dart';

part 'status_response.g.dart';

@JsonSerializable(explicitToJson: true)
class StatusResponse {
  @JsonKey(name: 'groupId')
  int? groupId;
  @JsonKey(name: 'groupName')
  String? groupName;
  @JsonKey(name: 'id')
  int? id;

  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'description')
  String? description;

  StatusResponse();

  Map<String, dynamic> toJson() => _$StatusResponseToJson(this);

  factory StatusResponse.fromJson(Map<String, dynamic> json) =>
      _$StatusResponseFromJson(json);
}
