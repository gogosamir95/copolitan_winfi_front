import 'package:copolitan_winfi/models/sendSMS/response/message_response.dart';
import 'package:json_annotation/json_annotation.dart';

part 'sms_response.g.dart';

@JsonSerializable(explicitToJson: true)
class SmsResponse {
  @JsonKey(name: 'messages')
  List<MessageResponse>? messageList;

  SmsResponse();

  Map<String, dynamic> toJson() => _$SmsResponseToJson(this);

  factory SmsResponse.fromJson(Map<String, dynamic> json) =>
      _$SmsResponseFromJson(json);
}
