import 'package:json_annotation/json_annotation.dart';

import 'message_request.dart';

part 'send_sms_request.g.dart';

@JsonSerializable(explicitToJson: true)
class SendSmsRequest {
  @JsonKey(name: 'messages')
  List<MessageRequest> messageList;

  SendSmsRequest({required this.messageList});

  Map<String, dynamic> toJson() => _$SendSmsRequestToJson(this);

  factory SendSmsRequest.fromJson(Map<String, dynamic> json) =>
      _$SendSmsRequestFromJson(json);
}
