import 'package:copolitan_winfi/models/sendSMS/request/destination_request.dart';
import 'package:json_annotation/json_annotation.dart';

part 'message_request.g.dart';

@JsonSerializable(explicitToJson: true)
class MessageRequest {
  @JsonKey(name: 'from')
  String from;
  @JsonKey(name: 'text')
  String message;
  @JsonKey(name: 'destinations')
  List<DestinationRequest> DestinationList;

  MessageRequest(
      {required this.from,
      required this.message,
      required this.DestinationList});

  Map<String, dynamic> toJson() => _$MessageRequestToJson(this);

  factory MessageRequest.fromJson(Map<String, dynamic> json) =>
      _$MessageRequestFromJson(json);
}
