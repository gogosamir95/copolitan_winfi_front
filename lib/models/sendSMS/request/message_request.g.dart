// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MessageRequest _$MessageRequestFromJson(Map<String, dynamic> json) =>
    MessageRequest(
      from: json['from'] as String,
      message: json['text'] as String,
      DestinationList: (json['destinations'] as List<dynamic>)
          .map((e) => DestinationRequest.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$MessageRequestToJson(MessageRequest instance) =>
    <String, dynamic>{
      'from': instance.from,
      'text': instance.message,
      'destinations': instance.DestinationList.map((e) => e.toJson()).toList(),
    };
