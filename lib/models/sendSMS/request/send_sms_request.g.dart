// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'send_sms_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SendSmsRequest _$SendSmsRequestFromJson(Map<String, dynamic> json) =>
    SendSmsRequest(
      messageList: (json['messages'] as List<dynamic>)
          .map((e) => MessageRequest.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$SendSmsRequestToJson(SendSmsRequest instance) =>
    <String, dynamic>{
      'messages': instance.messageList.map((e) => e.toJson()).toList(),
    };
