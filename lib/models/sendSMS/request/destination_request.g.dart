// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'destination_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

DestinationRequest _$DestinationRequestFromJson(Map<String, dynamic> json) =>
    DestinationRequest(
      to: json['to'] as String,
    );

Map<String, dynamic> _$DestinationRequestToJson(DestinationRequest instance) =>
    <String, dynamic>{
      'to': instance.to,
    };
