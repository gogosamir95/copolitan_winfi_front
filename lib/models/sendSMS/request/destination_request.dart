import 'package:json_annotation/json_annotation.dart';

part 'destination_request.g.dart';

@JsonSerializable(explicitToJson: true)
class DestinationRequest {
  @JsonKey(name: 'to')
  String to;

  DestinationRequest({required this.to});

  Map<String, dynamic> toJson() => _$DestinationRequestToJson(this);

  factory DestinationRequest.fromJson(Map<String, dynamic> json) =>
      _$DestinationRequestFromJson(json);
}
