// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'header_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

HeaderResponse _$HeaderResponseFromJson(Map<String, dynamic> json) =>
    HeaderResponse()
      ..success = json['success'] as bool?
      ..message = json['msg'] as String?
      ..data = json['data'];

Map<String, dynamic> _$HeaderResponseToJson(HeaderResponse instance) =>
    <String, dynamic>{
      'success': instance.success,
      'msg': instance.message,
      'data': instance.data,
    };
