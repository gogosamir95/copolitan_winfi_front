// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verify_user_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VerifyUserRequest _$VerifyUserRequestFromJson(Map<String, dynamic> json) =>
    VerifyUserRequest(
      json['mac'] as String?,
      json['id'] as String?,
    );

Map<String, dynamic> _$VerifyUserRequestToJson(VerifyUserRequest instance) =>
    <String, dynamic>{
      'mac': instance.mac,
      'id': instance.id,
    };
