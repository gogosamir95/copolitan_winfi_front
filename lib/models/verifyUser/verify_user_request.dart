import 'package:json_annotation/json_annotation.dart';

part 'verify_user_request.g.dart';

@JsonSerializable(explicitToJson: true)
class VerifyUserRequest {
  @JsonKey(name: 'mac')
  String? mac;
  @JsonKey(name: 'id')
  String? id;

  VerifyUserRequest(this.mac, this.id);

  Map<String, dynamic> toJson() => _$VerifyUserRequestToJson(this);

  factory VerifyUserRequest.fromJson(Map<String, dynamic> json) =>
      _$VerifyUserRequestFromJson(json);
}
