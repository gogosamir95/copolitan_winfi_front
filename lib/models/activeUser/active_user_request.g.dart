// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'active_user_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ActiveUserRequest _$ActiveUserRequestFromJson(Map<String, dynamic> json) =>
    ActiveUserRequest(
      json['username'] as String,
      json['password'] as String,
    );

Map<String, dynamic> _$ActiveUserRequestToJson(ActiveUserRequest instance) =>
    <String, dynamic>{
      'username': instance.userName,
      'password': instance.password,
    };
