import 'package:json_annotation/json_annotation.dart';

part 'active_user_request.g.dart';

@JsonSerializable(explicitToJson: true)
class ActiveUserRequest {
  @JsonKey(name: 'username')
  String userName;
  @JsonKey(name: 'password')
  String password;

  ActiveUserRequest(this.userName, this.password);

  Map<String, dynamic> toJson() => _$ActiveUserRequestToJson(this);

  factory ActiveUserRequest.fromJson(Map<String, dynamic> json) =>
      _$ActiveUserRequestFromJson(json);
}
