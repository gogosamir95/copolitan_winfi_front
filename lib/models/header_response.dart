import 'package:json_annotation/json_annotation.dart';

part 'header_response.g.dart';

@JsonSerializable(explicitToJson: true)
class HeaderResponse {
  @JsonKey(name: 'success')
  bool? success;
  @JsonKey(name: 'msg')
  String? message;
  @JsonKey(name: 'data')
  dynamic data;

  HeaderResponse();

  Map<String, dynamic> toJson() => _$HeaderResponseToJson(this);

  factory HeaderResponse.fromJson(Map<String, dynamic> json) =>
      _$HeaderResponseFromJson(json);
}
