import 'package:json_annotation/json_annotation.dart';

part 'user_response.g.dart';

@JsonSerializable(explicitToJson: true)
class UserResponse {
  @JsonKey(name: 'id')
  int? id;
  @JsonKey(name: 'email')
  String? email;
  @JsonKey(name: 'first_name')
  String? firstName;
  @JsonKey(name: 'last_name')
  String? lastName;
  @JsonKey(name: 'display_name')
  String? displayName;
  @JsonKey(name: 'password')
  String? password;
  @JsonKey(name: 'mobile')
  String? mobile;
  @JsonKey(name: 'random_code')
  String? randomCode;

  UserResponse();

  Map<String, dynamic> toJson() => _$UserResponseToJson(this);

  factory UserResponse.fromJson(Map<String, dynamic> json) =>
      _$UserResponseFromJson(json);
}
