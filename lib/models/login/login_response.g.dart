// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

LoginResponse _$LoginResponseFromJson(Map<String, dynamic> json) =>
    LoginResponse()
      ..otp = json['otp'] as String?
      ..userResponse = json['user'] == null
          ? null
          : UserResponse.fromJson(json['user'] as Map<String, dynamic>);

Map<String, dynamic> _$LoginResponseToJson(LoginResponse instance) =>
    <String, dynamic>{
      'otp': instance.otp,
      'user': instance.userResponse?.toJson(),
    };
