import 'package:copolitan_winfi/models/login/login_response.dart';

class LoginMapper {
  var _id,
      _firstName,
      _lastName,
      _email,
      _mobile,
      _password,
      _displayName,
      _otp;

  int get id => _id;

  String get firstName => _firstName;

  String get lastName => _lastName;

  String get email => _email;

  String get mobile => _mobile;

  String get password => _password;

  String get displayName => _displayName;

  String get otp => _otp;

  LoginMapper(LoginResponse response) {
    _id = response.userResponse!.id;
    _firstName = response.userResponse!.firstName;
    _lastName = response.userResponse!.lastName;
    _email = response.userResponse!.email;
    _mobile = response.userResponse!.mobile;
    _password = response.userResponse!.randomCode ?? '';
    _displayName = response.userResponse!.displayName;
    _otp = response.otp;
  }
}
