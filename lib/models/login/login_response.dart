import 'package:json_annotation/json_annotation.dart';

import 'user_response.dart';

part 'login_response.g.dart';

@JsonSerializable(explicitToJson: true)
class LoginResponse {
  @JsonKey(name: 'otp')
  String? otp;
  @JsonKey(name: 'user')
  UserResponse? userResponse;

  LoginResponse();

  Map<String, dynamic> toJson() => _$LoginResponseToJson(this);

  factory LoginResponse.fromJson(Map<String, dynamic> json) =>
      _$LoginResponseFromJson(json);
}
