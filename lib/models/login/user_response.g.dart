// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

UserResponse _$UserResponseFromJson(Map<String, dynamic> json) => UserResponse()
  ..id = json['id'] as int?
  ..email = json['email'] as String?
  ..firstName = json['first_name'] as String?
  ..lastName = json['last_name'] as String?
  ..displayName = json['display_name'] as String?
  ..password = json['password'] as String?
  ..mobile = json['mobile'] as String?
  ..randomCode = json['random_code'] as String?;

Map<String, dynamic> _$UserResponseToJson(UserResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'email': instance.email,
      'first_name': instance.firstName,
      'last_name': instance.lastName,
      'display_name': instance.displayName,
      'password': instance.password,
      'mobile': instance.mobile,
      'random_code': instance.randomCode,
    };
