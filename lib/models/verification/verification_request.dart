import 'package:json_annotation/json_annotation.dart';

part 'verification_request.g.dart';

@JsonSerializable(explicitToJson: true)
class VerificationRequest {
  @JsonKey(name: 'otp')
  String otp;
  @JsonKey(name: 'macAddress')
  String macAddress;
  @JsonKey(name: 'browser')
  String browser;
  @JsonKey(name: 'browserVersion')
  String browserVersion;

  VerificationRequest(
      this.otp, this.macAddress, this.browser, this.browserVersion);

  Map<String, dynamic> toJson() => _$VerificationRequestToJson(this);

  factory VerificationRequest.fromJson(Map<String, dynamic> json) =>
      _$VerificationRequestFromJson(json);
}
