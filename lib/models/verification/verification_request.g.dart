// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'verification_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VerificationRequest _$VerificationRequestFromJson(Map<String, dynamic> json) =>
    VerificationRequest(
      json['otp'] as String,
      json['macAddress'] as String,
      json['browser'] as String,
      json['browserVersion'] as String,
    );

Map<String, dynamic> _$VerificationRequestToJson(
        VerificationRequest instance) =>
    <String, dynamic>{
      'otp': instance.otp,
      'macAddress': instance.macAddress,
      'browser': instance.browser,
      'browserVersion': instance.browserVersion,
    };
