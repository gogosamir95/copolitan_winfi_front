import 'package:copolitan_winfi/models/call/voice_request.dart';
import 'package:json_annotation/json_annotation.dart';

part 'call_request.g.dart';

@JsonSerializable(explicitToJson: true)
class CallRequest {
  @JsonKey(name: 'text')
  String? text;
  @JsonKey(name: 'language')
  String? language;
  @JsonKey(name: 'voice')
  VoiceRequest? voiceRequest;
  @JsonKey(name: 'from')
  String? from;
  @JsonKey(name: 'to')
  String? to;
  @JsonKey(name: 'pause')
  int? pause;
  @JsonKey(name: 'speechRate')
  int? speechRate;

  CallRequest(
      {this.text,
      this.language,
      this.voiceRequest,
      this.from,
      this.to,
      this.pause,
      this.speechRate});

  Map<String, dynamic> toJson() => _$CallRequestToJson(this);

  factory CallRequest.fromJson(Map<String, dynamic> json) =>
      _$CallRequestFromJson(json);
}
