// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'call_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CallRequest _$CallRequestFromJson(Map<String, dynamic> json) => CallRequest(
      text: json['text'] as String?,
      language: json['language'] as String?,
      voiceRequest: json['voice'] == null
          ? null
          : VoiceRequest.fromJson(json['voice'] as Map<String, dynamic>),
      from: json['from'] as String?,
      to: json['to'] as String?,
      pause: json['pause'] as int?,
      speechRate: json['speechRate'] as int?,
    );

Map<String, dynamic> _$CallRequestToJson(CallRequest instance) =>
    <String, dynamic>{
      'text': instance.text,
      'language': instance.language,
      'voice': instance.voiceRequest?.toJson(),
      'from': instance.from,
      'to': instance.to,
      'pause': instance.pause,
      'speechRate': instance.speechRate,
    };
