import 'package:json_annotation/json_annotation.dart';

part 'voice_request.g.dart';

@JsonSerializable(explicitToJson: true)
class VoiceRequest {
  @JsonKey(name: 'name')
  String? name;
  @JsonKey(name: 'gender')
  String? gender;

  VoiceRequest({this.name, this.gender});

  Map<String, dynamic> toJson() => _$VoiceRequestToJson(this);

  factory VoiceRequest.fromJson(Map<String, dynamic> json) =>
      _$VoiceRequestFromJson(json);
}
