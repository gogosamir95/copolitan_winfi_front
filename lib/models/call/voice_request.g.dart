// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'voice_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

VoiceRequest _$VoiceRequestFromJson(Map<String, dynamic> json) => VoiceRequest(
      name: json['name'] as String?,
      gender: json['gender'] as String?,
    );

Map<String, dynamic> _$VoiceRequestToJson(VoiceRequest instance) =>
    <String, dynamic>{
      'name': instance.name,
      'gender': instance.gender,
    };
