import 'package:json_annotation/json_annotation.dart';

part 'call_response.g.dart';

@JsonSerializable(explicitToJson: true)
class CallResponse {
  @JsonKey(name: 'bulkId')
  String? bulkId;

  CallResponse();

  Map<String, dynamic> toJson() => _$CallResponseToJson(this);

  factory CallResponse.fromJson(Map<String, dynamic> json) =>
      _$CallResponseFromJson(json);
}
