import 'package:json_annotation/json_annotation.dart';

part 'check_user_exist_request.g.dart';

@JsonSerializable(explicitToJson: true)
class CheckUserExistRequest {
  @JsonKey(name: 'mac')
  String? mac;

  CheckUserExistRequest(this.mac);

  Map<String, dynamic> toJson() => _$CheckUserExistRequestToJson(this);

  factory CheckUserExistRequest.fromJson(Map<String, dynamic> json) =>
      _$CheckUserExistRequestFromJson(json);
}
