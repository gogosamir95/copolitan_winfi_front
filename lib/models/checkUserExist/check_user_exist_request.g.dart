// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'check_user_exist_request.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CheckUserExistRequest _$CheckUserExistRequestFromJson(
        Map<String, dynamic> json) =>
    CheckUserExistRequest(
      json['mac'] as String?,
    );

Map<String, dynamic> _$CheckUserExistRequestToJson(
        CheckUserExistRequest instance) =>
    <String, dynamic>{
      'mac': instance.mac,
    };
