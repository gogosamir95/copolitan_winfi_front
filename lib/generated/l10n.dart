// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Login`
  String get login {
    return Intl.message(
      'Login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `Loading`
  String get loading {
    return Intl.message(
      'Loading',
      name: 'loading',
      desc: '',
      args: [],
    );
  }

  /// `Success`
  String get success {
    return Intl.message(
      'Success',
      name: 'success',
      desc: '',
      args: [],
    );
  }

  /// `Failed`
  String get failed {
    return Intl.message(
      'Failed',
      name: 'failed',
      desc: '',
      args: [],
    );
  }

  /// `Mobile Number`
  String get mobileNumber {
    return Intl.message(
      'Mobile Number',
      name: 'mobileNumber',
      desc: '',
      args: [],
    );
  }

  /// `Copolitan Coffice`
  String get copolitanCoffice {
    return Intl.message(
      'Copolitan Coffice',
      name: 'copolitanCoffice',
      desc: '',
      args: [],
    );
  }

  /// `+20`
  String get defaultCountryCode {
    return Intl.message(
      '+20',
      name: 'defaultCountryCode',
      desc: '',
      args: [],
    );
  }

  /// `No data found!`
  String get noDataFound {
    return Intl.message(
      'No data found!',
      name: 'noDataFound',
      desc: '',
      args: [],
    );
  }

  /// `OK`
  String get ok {
    return Intl.message(
      'OK',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `Error`
  String get error {
    return Intl.message(
      'Error',
      name: 'error',
      desc: '',
      args: [],
    );
  }

  /// `Required`
  String get required {
    return Intl.message(
      'Required',
      name: 'required',
      desc: '',
      args: [],
    );
  }

  /// `Invalid OTP`
  String get invalidOtp {
    return Intl.message(
      'Invalid OTP',
      name: 'invalidOtp',
      desc: '',
      args: [],
    );
  }

  /// `Email required`
  String get emailRequired {
    return Intl.message(
      'Email required',
      name: 'emailRequired',
      desc: '',
      args: [],
    );
  }

  /// `Invalid email`
  String get invalidEmail {
    return Intl.message(
      'Invalid email',
      name: 'invalidEmail',
      desc: '',
      args: [],
    );
  }

  /// `Mobile required`
  String get mobileRequired {
    return Intl.message(
      'Mobile required',
      name: 'mobileRequired',
      desc: '',
      args: [],
    );
  }

  /// `Invalid mobile number`
  String get invalidMobile {
    return Intl.message(
      'Invalid mobile number',
      name: 'invalidMobile',
      desc: '',
      args: [],
    );
  }

  /// `password required`
  String get passwordRequired {
    return Intl.message(
      'password required',
      name: 'passwordRequired',
      desc: '',
      args: [],
    );
  }

  /// `Invalid password`
  String get invalidPassword {
    return Intl.message(
      'Invalid password',
      name: 'invalidPassword',
      desc: '',
      args: [],
    );
  }

  /// `Powered By WINFI`
  String get poweredByWinfi {
    return Intl.message(
      'Powered By WINFI',
      name: 'poweredByWinfi',
      desc: '',
      args: [],
    );
  }

  /// `Login to your account`
  String get loginToYourAccount {
    return Intl.message(
      'Login to your account',
      name: 'loginToYourAccount',
      desc: '',
      args: [],
    );
  }

  /// `Verification Code`
  String get verificationCode {
    return Intl.message(
      'Verification Code',
      name: 'verificationCode',
      desc: '',
      args: [],
    );
  }

  /// `Please enter verification code sent to (+20){mobile}`
  String pleaseEnterVerificationCodeSentTo(Object mobile) {
    return Intl.message(
      'Please enter verification code sent to (+20)$mobile',
      name: 'pleaseEnterVerificationCodeSentTo',
      desc: '',
      args: [mobile],
    );
  }

  /// `Didn't receive your code?`
  String get didNotReceiveCodeYet {
    return Intl.message(
      'Didn\'t receive your code?',
      name: 'didNotReceiveCodeYet',
      desc: '',
      args: [],
    );
  }

  /// `Resend Via`
  String get resendVa {
    return Intl.message(
      'Resend Via',
      name: 'resendVa',
      desc: '',
      args: [],
    );
  }

  /// `Automated Call`
  String get automatedCall {
    return Intl.message(
      'Automated Call',
      name: 'automatedCall',
      desc: '',
      args: [],
    );
  }

  /// `Or`
  String get or {
    return Intl.message(
      'Or',
      name: 'or',
      desc: '',
      args: [],
    );
  }

  /// `SMS`
  String get sms {
    return Intl.message(
      'SMS',
      name: 'sms',
      desc: '',
      args: [],
    );
  }

  /// `verify & connect`
  String get verifyAndConnect {
    return Intl.message(
      'verify & connect',
      name: 'verifyAndConnect',
      desc: '',
      args: [],
    );
  }

  /// `Invalid verification code`
  String get invalidOTP {
    return Intl.message(
      'Invalid verification code',
      name: 'invalidOTP',
      desc: '',
      args: [],
    );
  }

  /// `Please try again in`
  String get pleaseTryAgainIn {
    return Intl.message(
      'Please try again in',
      name: 'pleaseTryAgainIn',
      desc: '',
      args: [],
    );
  }

  /// `Edit`
  String get edit {
    return Intl.message(
      'Edit',
      name: 'edit',
      desc: '',
      args: [],
    );
  }

  /// `Please wait`
  String get pleaseWait {
    return Intl.message(
      'Please wait',
      name: 'pleaseWait',
      desc: '',
      args: [],
    );
  }

  /// `OTP has been sent to ${mobile}`
  String messageSentAgain(Object mobile) {
    return Intl.message(
      'OTP has been sent to \$$mobile',
      name: 'messageSentAgain',
      desc: '',
      args: [mobile],
    );
  }

  /// `Book at Copolitan`
  String get bookAtCopolitan {
    return Intl.message(
      'Book at Copolitan',
      name: 'bookAtCopolitan',
      desc: '',
      args: [],
    );
  }

  /// `Or simply download our app`
  String get downloadApp {
    return Intl.message(
      'Or simply download our app',
      name: 'downloadApp',
      desc: '',
      args: [],
    );
  }

  /// `IOS`
  String get IOS {
    return Intl.message(
      'IOS',
      name: 'IOS',
      desc: '',
      args: [],
    );
  }

  /// `Android`
  String get android {
    return Intl.message(
      'Android',
      name: 'android',
      desc: '',
      args: [],
    );
  }

  /// `Welcome Back!`
  String get welcomeBack {
    return Intl.message(
      'Welcome Back!',
      name: 'welcomeBack',
      desc: '',
      args: [],
    );
  }

  /// `Connect to internet`
  String get connectToInternet {
    return Intl.message(
      'Connect to internet',
      name: 'connectToInternet',
      desc: '',
      args: [],
    );
  }

  /// `Change my account`
  String get changeMyAccount {
    return Intl.message(
      'Change my account',
      name: 'changeMyAccount',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
