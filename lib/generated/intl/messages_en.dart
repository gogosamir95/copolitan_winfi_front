// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(mobile) => "OTP has been sent to \$${mobile}";

  static String m1(mobile) =>
      "Please enter verification code sent to (+20)${mobile}";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "IOS": MessageLookupByLibrary.simpleMessage("IOS"),
        "android": MessageLookupByLibrary.simpleMessage("Android"),
        "automatedCall": MessageLookupByLibrary.simpleMessage("Automated Call"),
        "bookAtCopolitan":
            MessageLookupByLibrary.simpleMessage("Book at Copolitan"),
        "changeMyAccount":
            MessageLookupByLibrary.simpleMessage("Change my account"),
        "connectToInternet":
            MessageLookupByLibrary.simpleMessage("Connect to internet"),
        "copolitanCoffice":
            MessageLookupByLibrary.simpleMessage("Copolitan Coffice"),
        "defaultCountryCode": MessageLookupByLibrary.simpleMessage("+20"),
        "didNotReceiveCodeYet":
            MessageLookupByLibrary.simpleMessage("Didn\'t receive your code?"),
        "downloadApp":
            MessageLookupByLibrary.simpleMessage("Or simply download our app"),
        "edit": MessageLookupByLibrary.simpleMessage("Edit"),
        "emailRequired": MessageLookupByLibrary.simpleMessage("Email required"),
        "error": MessageLookupByLibrary.simpleMessage("Error"),
        "failed": MessageLookupByLibrary.simpleMessage("Failed"),
        "invalidEmail": MessageLookupByLibrary.simpleMessage("Invalid email"),
        "invalidMobile":
            MessageLookupByLibrary.simpleMessage("Invalid mobile number"),
        "invalidOTP":
            MessageLookupByLibrary.simpleMessage("Invalid verification code"),
        "invalidOtp": MessageLookupByLibrary.simpleMessage("Invalid OTP"),
        "invalidPassword":
            MessageLookupByLibrary.simpleMessage("Invalid password"),
        "loading": MessageLookupByLibrary.simpleMessage("Loading"),
        "login": MessageLookupByLibrary.simpleMessage("Login"),
        "loginToYourAccount":
            MessageLookupByLibrary.simpleMessage("Login to your account"),
        "messageSentAgain": m0,
        "mobileNumber": MessageLookupByLibrary.simpleMessage("Mobile Number"),
        "mobileRequired":
            MessageLookupByLibrary.simpleMessage("Mobile required"),
        "noDataFound": MessageLookupByLibrary.simpleMessage("No data found!"),
        "ok": MessageLookupByLibrary.simpleMessage("OK"),
        "or": MessageLookupByLibrary.simpleMessage("Or"),
        "passwordRequired":
            MessageLookupByLibrary.simpleMessage("password required"),
        "pleaseEnterVerificationCodeSentTo": m1,
        "pleaseTryAgainIn":
            MessageLookupByLibrary.simpleMessage("Please try again in"),
        "pleaseWait": MessageLookupByLibrary.simpleMessage("Please wait"),
        "poweredByWinfi":
            MessageLookupByLibrary.simpleMessage("Powered By WINFI"),
        "required": MessageLookupByLibrary.simpleMessage("Required"),
        "resendVa": MessageLookupByLibrary.simpleMessage("Resend Via"),
        "sms": MessageLookupByLibrary.simpleMessage("SMS"),
        "success": MessageLookupByLibrary.simpleMessage("Success"),
        "verificationCode":
            MessageLookupByLibrary.simpleMessage("Verification Code"),
        "verifyAndConnect":
            MessageLookupByLibrary.simpleMessage("verify & connect"),
        "welcomeBack": MessageLookupByLibrary.simpleMessage("Welcome Back!")
      };
}
