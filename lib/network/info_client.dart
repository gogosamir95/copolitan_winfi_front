import 'package:copolitan_winfi/models/activeUser/active_user_request.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

part 'info_client.g.dart';

@RestApi(baseUrl: 'https://wifi.winfico.com')
abstract class InfoClient {
  factory InfoClient(Dio dio) = _InfoClient;

  @POST('/login', autoCastResponse: false)
  Future<void> activeInternet(@Body() ActiveUserRequest request);
}
