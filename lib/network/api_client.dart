import 'package:copolitan_winfi/models/checkUserExist/check_user_exist_request.dart';
import 'package:copolitan_winfi/models/header_response.dart';
import 'package:copolitan_winfi/models/login/login_request.dart';
import 'package:copolitan_winfi/models/verification/verification_request.dart';
import 'package:copolitan_winfi/models/verifyUser/verify_user_request.dart';
import 'package:copolitan_winfi/utlities/constant.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

part 'api_client.g.dart';

@RestApi(
    baseUrl: Constant.appLive ? Constant.baseDebugUrl : Constant.baseLiveUrl)
abstract class ApiClient {
  factory ApiClient(Dio dio) = _ApiClient;

  @POST('api/v1/user/generateOtp')
  Future<HeaderResponse> login(@Body() LoginRequest request);

  @POST('api/v1/user/login')
  Future<HeaderResponse> verifyUser(@Body() VerificationRequest request);

  @POST('api/v1/user/connectUser')
  Future<HeaderResponse> checkMacExist(@Body() CheckUserExistRequest request);

  @POST('api/v1/user/checkUserMacAndId')
  Future<HeaderResponse> verifyUserWithMacAddress(
      @Body() VerifyUserRequest request);

  @POST('api/v1/user/expiryUserIdAndMac')
  Future<HeaderResponse> expireUser(@Body() VerifyUserRequest request);
}
