import 'dart:convert';

import 'package:copolitan_winfi/utlities/constant.dart';
import 'package:copolitan_winfi/utlities/logger.dart';
import 'package:dio/dio.dart';

const String _infoBipKey =
    'App 6cf83e39d404bc8caa0e8a85ce2b6327-841b3e30-b82f-4758-a965-08935ec73cbc';

class InfoBipDioHelper {
  static InfoBipDioHelper _instance = InfoBipDioHelper.internal();
  static Dio? _dio;
  static int _connectionTimeOut = 5000000;

  InfoBipDioHelper.internal();

  factory InfoBipDioHelper() {
    return _instance;
  }

  Dio getDio() {
    if (_dio == null) {
      _dio = Dio();
      _dio!.options = _defaultBaseOption;
      _updateBearerToken;
      _dio!.interceptors.add(_interceptorsWrapper);
    }
    return _dio!;
  }

  void get _updateBearerToken =>
      _dio!.options.headers['Authorization'] = '$_infoBipKey';

  BaseOptions get _defaultBaseOption => BaseOptions(
      baseUrl: Constant.appLive ? Constant.baseLiveUrl : Constant.baseDebugUrl,
      connectTimeout: _connectionTimeOut,
      receiveTimeout: _connectionTimeOut,
      sendTimeout: _connectionTimeOut);

  InterceptorsWrapper get _interceptorsWrapper => InterceptorsWrapper(
        onError: (e, handler) async {
          Logger.log(
              message: e.message,
              name: 'info bip error response:',
              stackTrace: e.stackTrace,
              error: e.error);
          return handler.next(e);
        },
        onRequest: (options, handler) {
          Logger.log(
              message: 'info bip REQUEST: json: ${json.encode(options.data)}',
              name: options.baseUrl + options.path);
          return handler.next(options);
        },
        onResponse: (response, handler) {
          Logger.log(
              message: 'info bip RESPONSE: ${json.encode(response.data)}',
              name: response.statusMessage!);
          return handler.next(response);
        },
      );
}
