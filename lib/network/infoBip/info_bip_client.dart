import 'package:copolitan_winfi/models/call/call_request.dart';
import 'package:copolitan_winfi/models/call/call_response.dart';
import 'package:copolitan_winfi/models/sendSMS/request/send_sms_request.dart';
import 'package:copolitan_winfi/models/sendSMS/response/sms_response.dart';
import 'package:copolitan_winfi/utlities/constant.dart';
import 'package:dio/dio.dart';
import 'package:retrofit/http.dart';

part 'info_bip_client.g.dart';

@RestApi(baseUrl: Constant.infoBipBaseUrl)
abstract class InfoBipClient {
  factory InfoBipClient(Dio dio) = _InfoBipClient;

  @POST('sms/2/text/advanced')
  Future<SmsResponse> sms(@Body() SendSmsRequest request);

  @POST('tts/3/single')
  Future<CallResponse> sendCall(@Body() CallRequest callRequest);
}
