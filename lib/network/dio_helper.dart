import 'dart:convert';

import 'package:copolitan_winfi/utlities/logger.dart';
import 'package:dio/adapter_browser.dart';
import 'package:dio/dio.dart';
import 'package:dio_http2_adapter/dio_http2_adapter.dart';

class DioHelper {
  static DioHelper _instance = DioHelper.internal();
  static Dio? _dio;
  static int _connectionTimeOut = 5000000;

  DioHelper.internal();

  factory DioHelper() {
    return _instance;
  }

  Dio getDio() {
    if (_dio == null) {
      _dio = Dio();
      _dio!.options = _defaultBaseOption;
      _dio!.httpClientAdapter = BrowserHttpClientAdapter();
      _dio!.interceptors.add(_interceptorsWrapper);
    }
    return _dio!;
  }

  Http2Adapter get _htt2Adapter => Http2Adapter(
        ConnectionManager(
          idleTimeout: 10000,
          // Ignore bad certificate
          onClientCreate: (_, config) => config.onBadCertificate = (_) => true,
        ),
      );

  BaseOptions get _defaultBaseOption => BaseOptions(
      connectTimeout: _connectionTimeOut,
      receiveTimeout: _connectionTimeOut,
      sendTimeout: _connectionTimeOut);

  InterceptorsWrapper get _interceptorsWrapper => InterceptorsWrapper(
        onError: (e, handler) async {
          Logger.log(
              message: e.message,
              name: 'error response:',
              stackTrace: e.stackTrace,
              error: e.error);
          return handler.next(e);
        },
        onRequest: (options, handler) {
          Logger.log(
              message: 'REQUEST: json: ${json.encode(options.data)}'
                  '\t query parameters: ${json.encode(options.queryParameters)}',
              name: options.baseUrl + options.path);
          return handler.next(options);
        },
        onResponse: (response, handler) {
          Logger.log(
              message: 'RESPONSE: ${json.encode(response.data)}',
              name: response.statusMessage!);
          return handler.next(response);
        },
      );
}
