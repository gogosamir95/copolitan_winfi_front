enum SharedPrefEnum {
  userId,
  isLight,
  language,
  userType,
  notification,
  firstTimeLogin,
  bearerToken,
  userName,
  profileUrl,
  active,
  customerId,
  BranchId,
  userEmail,
  userMobile,
  roomId
}
