import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/utlities/image_const.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';
import 'package:image_loader/image_helper.dart';

class EgyptRowWidget extends StatelessWidget {
  const EgyptRowWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => _getEgyptRow(context);

  Widget _getEgyptRow(BuildContext context) => Container(
        width: 100,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            SizedBox(
              width: 14,
            ),
            ImageHelper(
              image: egyptIcon,
              imageType: ImageType.asset,
              width: 14,
              height: 21,
            ),
            SizedBox(
              width: 2,
            ),
            CustomText(
                text: S.of(context).defaultCountryCode,
                customTextStyle: RegularStyle(color: greyColor, fontSize: 16)),
            SizedBox(
              width: 2,
            ),
            ImageHelper(
              image: arrowDownIcon,
              imageType: ImageType.svg,
              width: 5,
              height: 5,
            ),
          ],
        ),
      );
}
