import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/utlities/image_const.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';
import 'package:image_loader/image_helper.dart';

class PoweredByWinfi extends StatelessWidget {
  final MainAxisAlignment mainAxisAlignment;

  const PoweredByWinfi(
      {Key? key, this.mainAxisAlignment = MainAxisAlignment.start})
      : super(key: key);

  @override
  Widget build(BuildContext context) => _getPoweredByWinfi(context);

  Widget _getPoweredByWinfi(BuildContext context) => Row(
        mainAxisAlignment: mainAxisAlignment,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          ImageHelper(
            image: appLogo,
            imageType: ImageType.asset,
            width: 40,
            height: 45,
            boxFit: BoxFit.fill,
          ),
          SizedBox(
            width: 5,
          ),
          CustomText(
              text: S.of(context).poweredByWinfi,
              customTextStyle:
                  RegularStyle(color: winfiAccentColor, fontSize: 12))
        ],
      );
}
