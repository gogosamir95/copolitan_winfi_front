import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/utlities/image_const.dart';
import 'package:image_loader/image_helper.dart';

class CopolitanLogoWidget extends StatelessWidget {
  // final double width, height;

  const CopolitanLogoWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => copolitanLogo;

  Widget get copolitanLogo => ImageHelper(
        image: coPolitanBlockLogo,
        imageType: ImageType.svg,
        boxFit: BoxFit.contain,
      );
}
