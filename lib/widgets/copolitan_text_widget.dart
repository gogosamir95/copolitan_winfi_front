import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';

class CopolitanTextWidget extends StatelessWidget {
  final CustomTextStyle customTextStyle;

  const CopolitanTextWidget({Key? key, required this.customTextStyle})
      : super(key: key);

  @override
  Widget build(BuildContext context) => CustomText(
      text: S.of(context).copolitanCoffice, customTextStyle: customTextStyle);
}
