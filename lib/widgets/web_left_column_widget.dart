import 'package:copolitan_winfi/base/base_state.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/widgets/copolitan_logo_widget.dart';
import 'package:copolitan_winfi/widgets/copolitan_text_widget.dart';
import 'package:copolitan_winfi/widgets/powered_by_winfi_widget.dart';

class WebLeftColumnWidget extends StatelessWidget {
  const WebLeftColumnWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => _leftColumn(context);

  Widget _leftColumn(BuildContext context) => Expanded(
        child: _getColumnContainer(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              getAnimatedContainer(
                  child: CopolitanLogoWidget(), context: context),
              SizedBox(
                height: 10,
              ),
              getAnimatedContainer(
                child: CopolitanTextWidget(
                  customTextStyle: SemiBoldStyle(
                      color: blackColor,
                      fontSize: MediaQuery.of(context).size.width * 0.025),
                ),
                context: context,
              ),
              SizedBox(
                height: 15,
              ),
              getAnimatedContainer(child: PoweredByWinfi(), context: context),
            ],
          ),
        ),
        flex: 4,
      );

  Widget getAnimatedContainer(
          {required Widget child, required BuildContext context}) =>
      AnimatedContainer(
          duration: Duration(milliseconds: 300),
          alignment: Alignment.centerLeft,
          curve: Curves.fastOutSlowIn,
          child: Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.05),
            child: child,
          ));

  Widget _getColumnContainer({required Widget child}) => Container(
        decoration: BoxDecoration(color: primaryColor, boxShadow: [
          BoxShadow(
              color: blackColor19Percent,
              blurRadius: 32,
              spreadRadius: 12,
              offset: Offset(0, 2))
        ]),
        child: child,
      );
}
