import 'dart:html';

import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/repository/check_user_exist_repository.dart';
import 'package:copolitan_winfi/screens/login/login_screen.dart';
import 'package:copolitan_winfi/screens/welcomeBack/welcome_back_widget.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/logger.dart';
import 'package:copolitan_winfi/utlities/string_extension.dart';
import 'package:flutter/material.dart';

import 'models/app_helper.dart';
import 'my_app.dart';

Future<void> main() async {
  final mac = await getParams();
  WidgetsFlutterBinding.ensureInitialized();
  await initAppModel();
  await initMyApp(mac);
}

Future<String> getParams() {
  String mac = '';
  try {
    var uri = Uri.dataFromString(window.location.href);
    Map<String, String> params = uri.queryParameters;
    mac = params['mac'] ?? '';
    Logger.log(message: mac, name: 'mac');
  } catch (e) {}
  // return Future.value('A4:4E:31:98:96:68');
  return Future.value(mac);
}

Future<void> initAppModel() async {
  await AppHelper().init();
  // await InfoRepository().infoResponse;
  AppHelper().updateSystemUiOverLayStyle(primaryColor, Brightness.dark);
  return Future.value();
}

Future<void> initMyApp(String mac) async {
  final macResponse = await CheckUserExistRepository().getUserMac(mac: mac);
  runApp(MyApp(
    startWidget: macResponse is SuccessState
        ? WelcomeBackWidget(
            userName: macResponse.response!.firstName.capitalizeFirstLetter() +
                ' ' +
                macResponse.response!.lastName.capitalizeFirstLetter(),
            userId: macResponse.response!.id,
            password: macResponse.response!.password,
            mobile: macResponse.response!.mobile,
            macAddress: mac,
          )
        : LoginScreen(mac: mac),
  ));
}
