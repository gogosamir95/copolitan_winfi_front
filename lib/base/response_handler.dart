import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/models/api_state.dart';
import 'package:copolitan_winfi/utlities/app_color.dart';
import 'package:copolitan_winfi/utlities/custom_text_style.dart';
import 'package:copolitan_winfi/utlities/view_helper.dart';
import 'package:copolitan_winfi/widgets/custom_progress.dart';
import 'package:copolitan_winfi/widgets/custom_text.dart';
import 'package:custom_progress_button/custom_progress.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ResponseHandLer {
  dynamic checkResponseState(
    ApiState apiState,
    BuildContext context, {
    double loaderSize = 50,
    Color loaderColor = accentColor,
    double noDataFoundSize = 18,
    String? noDataFoundText,
    bool isUseExpanded = false,
    Widget? onSuccess,
    Widget? idleWidget,
  }) {
    if (apiState is LoadingState)
      return isUseExpanded
          ? getExpanded(getCustomProgress(
              loaderColor: loaderColor, loaderSize: loaderSize))
          : getCustomProgress(loaderColor: loaderColor, loaderSize: loaderSize);
    else if (apiState is SuccessState)
      return onSuccess;
    else if (apiState is FailedState)
      return getNoDataFound(context,
          textColor: loaderColor,
          noDataFoundText: noDataFoundText,
          isUseExpanded: isUseExpanded);
    else if (apiState is ErrorState)
      return getErrorText(apiState.error.toString());
    else if (apiState is IdleState)
      return idleWidget;
    else
      return isUseExpanded
          ? getExpanded(getCustomProgress(
              loaderColor: loaderColor, loaderSize: loaderSize))
          : getCustomProgress(loaderColor: loaderColor, loaderSize: loaderSize);
  }

  void checkResponseStateForButton(ApiState apiState, BuildContext context,
      {VoidCallback? onSuccess,
      required BehaviorSubject<String> failedBehaviour,
      required BehaviorSubject<ButtonState> buttonBehaviour}) {
    if (apiState is LoadingState)
      buttonBehaviour.sink.add(ButtonState.loading);
    else if (apiState is SuccessState) {
      buttonBehaviour.sink.add(ButtonState.success);
      onSuccess!();
    } else if (apiState is ErrorState) {
      buttonBehaviour.sink.add(ButtonState.idle);
      showErrorDialog(apiState.message, context);
      // failedBehaviour.sink.add(apiState.message);
    } else {
      buttonBehaviour.sink.add(ButtonState.idle);
      showErrorDialog(apiState.message, context);
      // failedBehaviour.sink.add(apiState.message);
    }
  }

  void showErrorDialog(String message, BuildContext context) {
    // ViewHelper(context).showCustomBottomSheetFullScreen(CustomAppDialog(
    //   headerString: S.of(context).error,
    //   headerTextColor: redColor,
    //   descriptionString: message,
    //   positiveClick: () {},
    //   positiveString: S.of(context).ok,
    //   negativeString: '',
    // ));
    ViewHelper(context).showSnakeBar(message);
  }

  Widget getCustomProgress({
    double loaderSize = 50,
    Color loaderColor = accentColor,
  }) =>
      CustomProgress(
        color: loaderColor,
        size: loaderSize,
      );

  Widget getExpanded(Widget child) => Expanded(child: child);

  Widget getErrorText(String error) {
    return Container(
        color: Colors.red,
        child: Center(
          child: Expanded(
            child: CustomText(
              text: error,
              customTextStyle: RegularStyle(color: redColor, fontSize: 15),
            ),
          ),
        ));
  }

  getNoDataFound(BuildContext context,
      {Color textColor = primaryColor,
      String? noDataFoundText,
      bool isUseExpanded = true}) {
    return isUseExpanded
        ? Expanded(
            flex: 1,
            child: noDataFound(
                textColor: textColor,
                noDataFoundText: noDataFoundText == null
                    ? S.of(context).noDataFound
                    : noDataFoundText),
          )
        : noDataFound(
            noDataFoundText: noDataFoundText == null
                ? S.of(context).noDataFound
                : noDataFoundText,
            textColor: textColor);
  }

  Widget noDataFound(
          {String? noDataFoundText, Color textColor = primaryColor}) =>
      Center(
        child: CustomText(
          text:
              noDataFoundText == null ? S.current.noDataFound : noDataFoundText,
          customTextStyle: RegularStyle(fontSize: 15, color: greyColor),
        ),
      );
}
