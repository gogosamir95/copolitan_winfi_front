import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:copolitan_winfi/utlities/constant.dart';
import 'package:flutter/material.dart';
import 'package:form_field_validator/form_field_validator.dart';

final MultiValidator emailValid = MultiValidator([
  RequiredValidator(errorText: S.current.emailRequired),
  EmailValidator(errorText: S.current.invalidEmail)
]);

final MultiValidator passwordValid = MultiValidator([
  RequiredValidator(errorText: S.current.passwordRequired),
  MinLengthValidator(8, errorText: S.current.invalidPassword),
]);

final MultiValidator mobileValid = MultiValidator([
  RequiredValidator(errorText: S.current.mobileRequired),
  PatternValidator(Constant.mobileRegex, errorText: S.current.invalidMobile),
]);

final MultiValidator emptyValid = MultiValidator([
  RequiredValidator(errorText: S.current.required),
]);

final MultiValidator otpValid = MultiValidator([
  RequiredValidator(errorText: S.current.required),
  MinLengthValidator(6, errorText: S.current.invalidOtp),
]);
final MatchValidator matchValid =
    MatchValidator(errorText: 'Not Match Password');

class Validator {
  /*final CreditCardValidator cardValidator = CreditCardValidator();*/
  MultiValidator emailValidator(BuildContext context) => MultiValidator([
        RequiredValidator(errorText: S.of(context).emailRequired),
        EmailValidator(errorText: S.of(context).invalidEmail)
      ]);

  MultiValidator mobileValidator(BuildContext context) => MultiValidator([
        RequiredValidator(errorText: S.of(context).mobileRequired),
        PatternValidator(Constant.mobileRegex,
            errorText: S.of(context).invalidMobile),
      ]);

  MultiValidator emptyValidator(BuildContext context) => MultiValidator([
        RequiredValidator(errorText: S.of(context).required),
      ]);

  MultiValidator passwordValidator(BuildContext context) => MultiValidator([
        RequiredValidator(errorText: S.of(context).passwordRequired),
        MinLengthValidator(8, errorText: S.of(context).invalidPassword),
      ]);

  MultiValidator otpValidator(
          BuildContext context, String otp, String validOtp) =>
      MultiValidator([
        RequiredValidator(errorText: S.current.passwordRequired),
        MinLengthValidator(6, errorText: S.current.invalidPassword),
      ]);

  MatchValidator matchValidator(BuildContext context) =>
      MatchValidator(errorText: 'Not Match Password');

  MultiValidator rangeValidator(
          BuildContext context, int maxValue, String message,
          {int minValue: 1}) =>
      MultiValidator([
        RequiredValidator(errorText: S.current.required),
        RangeValidator(
            min: minValue, max: maxValue, errorText: message + ' $maxValue'),
      ]);

  bool isPasswordValid(String? value) => passwordValid.isValid(value);

  bool notEmptyValid(String? value) => emptyValid.isValid(value);

  bool isEmailValid(String? value) => emailValid.isValid(value);

  bool isMobileValid(String? value) => mobileValid.isValid(value);

  bool isOtpValid(String? value) => otpValid.isValid(value);

  isMatchValid(String? value1, String? value2) =>
      matchValid.validateMatch(value1!, value2!);
}
