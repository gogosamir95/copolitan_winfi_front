import 'package:copolitan_winfi/generated/l10n.dart';
import 'package:custom_progress_button/custom_progress.dart';
import 'package:rxdart/rxdart.dart';

import 'bloc_base.dart';

class ButtonBloc extends BlocBase {
  final failedBehaviour = BehaviorSubject<String>();
  final buttonBehavior = BehaviorSubject<ButtonState>();

  void init() {
    buttonBehavior.sink.add(ButtonState.idle);
    failedBehaviour.sink.add(S.current.failed);
  }

  @override
  void dispose() {
    buttonBehavior.close();
    failedBehaviour.close();
  }
}
